![Tux!](Resources/UI/Logo.png)
A 3D platform game featuring the famous Linux mascot. ![Tux](https://luckey.games/images/Tux.png)

See the [guide](./Docs/Guide/TuxGuide_EN.md) for instructions.

# Screenshots

![](Screenshots/Screenshot_Fri_Nov_18_00_57_12_2022.png) 
  
![](Screenshots/Screenshot_Mon_Oct_31_22_04_05_2022.png)


# Licenses
Code is licensed [GNU GPL3](LICENSE), for the license of specific assets see [this list](LICENSE_ASSETS.md).
