## Used licenses

_[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)/[4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)/[4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various
##### Author irrelevant
- CC0
    + Docs/*
    + Screenshots/*
    + Resources/Materials/*
    + Resources/UI/*.xml 
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/Animations/*
    + Resources/PostProcess/*
    + Resources/Textures/LUT.*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Textures/Smoke.*

### Music
##### Lobo Loco
- CC-BY-SA 4.0
    + Resources/Music/Woke Up This Morning.ogg [| Source |](https://freemusicarchive.org/music/Lobo_Loco/harvest-times/woke-up-this-morning-theme-fma-podcast-suggestion/)

##### Heftone Banjo Orchestra
- CC-BY-SA 4.0
    + Resources/Music/Music Box Rag.ogg [| Source |](https://freemusicarchive.org/music/Heftone_Banjo_Orchestra/Music_Box_Rag/Heftone_Banjo_Orchestra_-_Music_Box_Rag_-_04_-_Music_Box_Rag/)

##### Jason Shaw
- CC-BY 3.0
    + Resources/Music/Rocky Top.ogg [| Source |](https://freemusicarchive.org/music/Jason_Shaw/Audionautix_Acoustic/ROCKY_TOP_traditional__1-44/)

##### Matthew Pablo (edited by Modanung)
- CC-BY 3.0
    + Resources/Music/Com Mecha.ogg [| Source |](https://opengameart.org/content/theme-of-com-mecha)
    
### Samples
#### CTCollab
- CC-BY 3.0
    + Resources/Samples/Splish.wav [| Source |](https://freesound.org/people/CTCollab/sounds/223807/)

##### Scrampunk
- CC-BY 4.0
    + Resources/Samples/Item.wav [| Source |](https://freesound.org/people/Scrampunk/sounds/345297/)

##### msantoro11 (split by Modanung)
- CC-BY 4.0
    + Resources/Samples/SnailA.wav [| Source |](https://freesound.org/people/msantoro11/sounds/351122/)
    + Resources/Samples/SnailB.wav [| Source |](https://freesound.org/people/msantoro11/sounds/351122/)
    
##### paep3nguin
- CC0
    + Resources/Samples/Click.wav [| Source |](https://freesound.org/people/paep3nguin/sounds/388046/)

##### mendihola (split by Modanung)
- CC0
    + Resources/Samples/Poof.wav [| Source |](https://freesound.org/people/mendihola/sounds/440269/)
    
##### SciFiSounds (split by Modanung)
- CC0
    + Resources/Samples/WhipStart.wav [| Source |](https://freesound.org/people/SciFiSounds/sounds/529925/)
    + Resources/Samples/WhipCrack.wav [| Source |](https://freesound.org/people/SciFiSounds/sounds/529925/)

##### colorsCrimsonTears
- CC0
    + Resources/Samples/Win.wav [| Source |](https://freesound.org/people/colorsCrimsonTears/sounds/566203/)

##### LittleRobotSoundFactory   
- CC0
    + Resources/Samples/Record.wav [| Source |](https://freesound.org/people/LittleRobotSoundFactory/sounds/270404/)
    
##### JustInvoke & Modanung
- CC-BY 4.0
    + Resources/Samples/Bounce.wav [| Source 1 |](https://freesound.org/people/JustInvoke/sounds/446104/) [| Source 2](https://gitlab.com/luckeyproductions/games/BlipNBlup/-/blob/master/Resources/Samples/Jump.wav) [(SC) |](https://gitlab.com/luckeyproductions/games/BlipNBlup/-/blob/master/raw/BlipnBlup.scd)
    
##### F.M.Audio (edited by Modanung)
- CC-BY 4.0
    + Resources/Samples/Door.wav [| Source |](https://freesound.org/people/F.M.Audio/sounds/555611/)

##### Hankof       
- CC0
    + Resources/Samples/Brick.wav [| Source |](https://freesound.org/people/Hankof/sounds/655621/)
    
##### jwb4 (edited by egomassive)
- CC0
    + Resources/Samples/Freeze.wav [| Source |](https://freesound.org/people/egomassive/sounds/536807/)
    
##### qubodup (edited by kwahmah_02 and Modanung)
- CC-BY 3.0
    + Resources/Samples/IceCast.wav [| Source |](https://freesound.org/people/kwahmah_02/sounds/259243/)
    
##### bspiller5 (edited by Modanung)
- CC0
    + Resources/Samples/FireCast.wav [| Source |](https://freesound.org/people/bspiller5/sounds/157616/)
    
##### Clearwavsound & Hyperfunction (mixed by Modanung)
- CC-BY 3.0
    + Resources/Samples/Sizzle.wav [| Source 1 |](https://freesound.org/people/Clearwavsound/sounds/541035/) [| Source 2 |](https://freesound.org/people/Hyperfunction/sounds/776368/)
    
##### Modanung
- CC-BY-SA 4.0
    + Resources/Samples/Hit.wav
    + Resources/Samples/Thud.wav
    + Resources/Samples/Warp.wav
        
### 2D   
##### Mms
- CC0
    + Resources/Fonts/Ftoonk.otf [| Source |](https://www.dafont.com/ftoonk.font)

##### Dwi Wahyu
- CC0
    + Resources/Fonts/ToysToss.otf [| Source |](https://www.dafont.com/toys-toss.font)

##### Khurasan
- CC0
    + Resources/Fonts/Boleh.otf [| Source |](https://www.dafont.com/boleh.font)
    
##### Modanung
- CC-BY-SA 4.0
    + Vector/*
    + Resources/UI/Arrow.png
    + Resources/UI/Heart.png
    + Resources/UI/Herring.png
    + Resources/UI/Key.png
    + Resources/UI/Logo.png
    + Resources/UI/Sardine.png
    + Resources/UI/Bullwhip.png
    + Resources/UI/Bricks.png
    + Resources/UI/Tools.png
    + Resources/UI/FireSpell.png (uses CC-BY-SA 3.0 [Debian logo](https://www.debian.org/logos/) by SPI Inc.)
    + Resources/UI/IceSpell.png
    + Resources/UI/Selector.png    
    + Resources/UI/Tux.png
    + Resources/UI/UI.png
    + Resources/Textures/TreeBark.png
    + Resources/Textures/TreeRings.png  
    + Resources/Textures/Grass.png
    + Resources/Textures/GrassBlades.png
    + Resources/Textures/Star.png

### 3D
##### Modanung
- CC-BY-SA 4.0
    + Blends/*
    + Resources/Models/*
- CC-BY 4.0
    + Resources/Maps/*
