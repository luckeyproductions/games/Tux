/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "jukebox.h"

JukeBox::JukeBox(Context* context): Object(context),
    musicNode_{ nullptr },
    tracks_{}
{
    Scene* musicScene{ new Scene(context_) };
    musicNode_ = musicScene->CreateChild("Music");

    SubscribeToEvent(E_MUSICFADED, DRY_HANDLER(JukeBox, HandleMusicFaded));
}

void JukeBox::Play(const String& song, bool loop, float gain)
{
    const float fadeTime{ .05f };

    for (SoundSource* source: tracks_)
    {
        if (fadeTime > 0.f)
        {
            SharedPtr<ValueAnimation> fade{ MakeShared<ValueAnimation>(context_) };
            fade->SetKeyFrame(0.f, { source->GetGain() });
            fade->SetKeyFrame(fadeTime, { .0f });
            fade->SetEventFrame(fadeTime, E_MUSICFADED);
            source->SetAttributeAnimation("Gain", fade, WM_ONCE);
        }
        else
        {
            tracks_.Remove(source);
            source->Remove();
        }
    }

    if (!song.IsEmpty())
    {
        Sound* music{ RES(Sound, String{ "Music/" } + song) };
        music->SetLooped(loop);
        SoundSource* musicSource{ musicNode_->CreateComponent<SoundSource>() };
        musicSource->SetSoundType(SOUND_MUSIC);
        musicSource->Play(music);
        musicSource->SetGain(gain);
        tracks_.Push(musicSource);
    }
}

void JukeBox::HandleMusicFaded(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SoundSource* source{ static_cast<SoundSource*>(GetEventSender()) };
    tracks_.Remove(source);
    source->Remove();
}
