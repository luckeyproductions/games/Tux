/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPAWNMASTER_H
#define SPAWNMASTER_H

#include "mastercontrol.h"

#define SPAWN GetSubsystem<SpawnMaster>()

class SpawnMaster: public Object
{
    DRY_OBJECT(SpawnMaster, Object);

public:
    SpawnMaster(Context* context);

    void Clear();

    template <class T> T* Create(bool recycle)
    {
        T* created{ nullptr };

        if (recycle)
        {
            PODVector<Node*> correctType{};
            GetScene()->GetChildrenWithComponent<T>(correctType);

            for (Node* n : correctType)
            {
                if (!n->IsEnabled())
                {
                    created = n->GetComponent<T>();
                    break;
                }
            }
        }

        if (!created)
        {
            Node* spawnedNode{ GetScene()->CreateChild(T::GetTypeNameStatic()) };
            created = spawnedNode->CreateComponent<T>();
        }

        return created;
    }

    template <class T> T* Create()
    {
        return Create<T>(true);
    }

    template <class T> T* Create(bool recycle, const Vector3& position, const Quaternion rotation = Quaternion::IDENTITY)
    {
        T* created{ Create<T>(recycle) };

        created->Set(position, rotation);

        return created;
    }

    template <class T> unsigned CountActive()
    {
        unsigned count{ 0u };
        PODVector<Node*> result{};
        GetScene()->GetChildrenWithComponent<T>(result, true);

        for (Node* r: result)
        {
            if (r->IsEnabled())
                ++count;
        }

        return count;
    }

    template <class T> void DisableAll()
    {
        if (!GetScene())
            return;

        PODVector<T*> result{};
        GetScene()->GetComponents<T>(result, true);

        for (T* r: result)
            r->Disable();
    }

    template <class T> void DisableAllDerived()
    {
        if (!GetScene())
            return;

        PODVector<T*> result{};
        GetScene()->GetDerivedComponents<T>(result, true);

        for (T* r: result)
            r->Disable();
    }


    Scene* GetScene() const;

    template <class T> T* GetNearest(const Vector3& position, float radius, unsigned mask = LAYER(L_STATIC))
    {
        PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
        PODVector<RigidBody*> result{};
        physics->GetRigidBodies(result, Sphere{ position, radius }, mask);
        T* nearest{ nullptr };
        float nearestDistance{ M_INFINITY };
        for (RigidBody* rb: result)
        {
            Node* node{ rb->GetNode() };
            const float distance{ rb->GetNode()->GetWorldPosition().DistanceToPoint(position) };
            if (node->HasComponent<T>())
            {
                if (distance < nearestDistance)
                {
                    nearest = node->GetComponent<T>();
                    nearestDistance = distance;
                }
            }
        }

        return nearest;
    }

private:
    void Activate();
    void Deactivate();
    void Restart();

    void HandleSceneUpdate(StringHash eventType, VariantMap &eventData);
};

#endif // SPAWNMASTER_H
