/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGSMENU_H
#define SETTINGSMENU_H

#include "gamemenu.h"

#define SLIDER_FACTOR 2.f

class SettingsMenu: public GameMenu
{
    DRY_OBJECT(SettingsMenu, GameMenu);

public:
    SettingsMenu(Context* context);

    void Hide() override;
    Button* BackButton() const { return backButton_; }

protected:
    void UpdateSizes() override;

private:
    UIElement* AttributesToInputList(Serializable* serializable);
    void FillResolutionsList(DropDownList* dropDownList, Serializable* serializable);
    void AddTitle(UIElement* panel, const String& text);

    void ApplyRenderPathSetting(const String attributeName);
    void ApplyAudioSetting(const String attributeName);

    void UpdateSizesOfPanel(UIElement* panel);

    void ModifySetting(StringHash eventType, VariantMap& eventData);
    void HandleApplyButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleCheckBoxHoverChanged(StringHash eventType, VariantMap& eventData);
    void HandleRowHoverChanged(StringHash eventType, VariantMap& eventData);
    void HandleBackButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleRowClicked(StringHash eventType, VariantMap& eventData);

    UIElement* graphicsPanel_;
    UIElement* audioPanel_;
    UIElement* controlsPanel_;
    Button* backButton_;
};

#endif // SETTINGSMENU_H
