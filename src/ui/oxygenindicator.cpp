/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gui.h"
#include "../tux.h"
#include "../effect/bubbles.h"
#include "../effectmaster.h"
#include "../game.h"
#include "healthindicator.h"

#include "oxygenindicator.h"

void OxygenIndicator::RegisterObject(Context* context)
{
    context->RegisterFactory<OxygenIndicator>();
    context->RegisterFactory<Bubbles>();

    DRY_COPY_BASE_ATTRIBUTES(Sprite);
}

OxygenIndicator::OxygenIndicator(Context* context): Sprite(context),
    bubble_{ nullptr },
    oxygen_{ -1 },
    breath_{ 0.f }
{
    context_->RegisterSubsystem(this);

    bubble_ = CreateChild<Sprite>("Bubble");
    bubble_->SetTexture(RES(Texture2D, "UI/Oxygen.png"));
    bubble_->SetImageRect({ 0, 0, 512, 512 });
    bubble_->SetSize(224, 224);
    bubble_->SetBlendMode(BLEND_ALPHA);
    bubble_->SetScale(Vector2::ZERO);

    SubscribeToEvent(E_SCENESUBSYSTEMUPDATE, DRY_HANDLER(OxygenIndicator, HandleSceneUpdate));
    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(OxygenIndicator, HandleSizesChanged));

    UpdateSizes();
}

void OxygenIndicator::SetShowOxygen(bool enabled)
{
    if (IsVisible() == enabled)
        return;

    if (enabled)
    {
        SetOxygen(-1);
        FillOxygen();
        SetVisible(true);
    }
    else
    {
        SetVisible(false);
    }
}

void OxygenIndicator::FillOxygen()
{
    SetOxygen(MAX_OXYGEN);
    breath_ = 1.f;
}

void OxygenIndicator::SetOxygen(int o2)
{
    if (o2 == oxygen_)
        return;

    Tux* tux{ GetSubsystem<Game>()->GetWorld().tux_ };

    if (!tux->HasScuba())
    {
        if (o2 != 0)
        {
            const bool animate{ o2 != -1 };
            bubble_->SetImageRect({ 0, 0, 512, 512 });

            if (animate)
            {
                const float animSpeed{ 2.3f / Sqrt(Abs(oxygen_ - o2)) };

                SharedPtr<ValueAnimation> scaleAnim{ context_->CreateObject<ValueAnimation>() };
                scaleAnim->SetKeyFrame(0.f, bubble_->GetScale());
                scaleAnim->SetKeyFrame(1.f, Vector2::ONE * OxygenToScale(o2));
                scaleAnim->SetInterpolationMethod(IM_SINUSOIDAL);
                bubble_->SetAttributeAnimation("Scale", scaleAnim, WM_CLAMP, animSpeed);

                SharedPtr<ValueAnimation> posAnim{ context_->CreateObject<ValueAnimation>() };
                posAnim->SetKeyFrame(0.f, bubble_->GetPosition());
                posAnim->SetKeyFrame(1.f, -.5f * bubble_->GetSize() * OxygenToScale(o2));
                posAnim->SetInterpolationMethod(IM_SINUSOIDAL);
                bubble_->SetAttributeAnimation("Position", posAnim, WM_CLAMP, animSpeed);
            }
            else
            {
                bubble_->RemoveAttributeAnimation("Scale");
                bubble_->SetScale(Vector2::ONE * OxygenToScale(o2));

                bubble_->RemoveAttributeAnimation("Position");
                bubble_->SetPosition(Vector2::ZERO);
            }
        }
        else
        {
            bubble_->SetImageRect({ 512, 0, 1024, 512 });
            const float animSpeed{ 17.f };

            SharedPtr<ValueAnimation> scaleAnim{ context_->CreateObject<ValueAnimation>() };
            scaleAnim->SetKeyFrame(0.f, bubble_->GetScale());
            scaleAnim->SetKeyFrame(.5f, Vector2::ONE * OxygenToScale(3));
            scaleAnim->SetKeyFrame(1.f, Vector2::ZERO);
            scaleAnim->SetInterpolationMethod(IM_SINUSOIDAL);
            bubble_->SetAttributeAnimation("Scale", scaleAnim, WM_CLAMP, animSpeed);

            SharedPtr<ValueAnimation> posAnim{ context_->CreateObject<ValueAnimation>() };
            posAnim->SetKeyFrame(0.f, bubble_->GetPosition());
            posAnim->SetKeyFrame(.5f, -.5f * bubble_->GetSize() * OxygenToScale(3));
            posAnim->SetKeyFrame(1.f, Vector2::ZERO);
            posAnim->SetInterpolationMethod(IM_SINUSOIDAL);
            bubble_->SetAttributeAnimation("Position", posAnim, WM_CLAMP, animSpeed);
        }
    }

    if (o2 < oxygen_ && o2 != -1)
        tux->GetGraphicsNode()->GetChild("Bubbles", true)->CreateComponent<Bubbles>();

    if (tux->HasScuba() && o2 >= 0)
        oxygen_ = MAX_OXYGEN;
    else
        oxygen_ = o2;
}

float OxygenIndicator::OxygenToScale(int o2)
{
    return Pow(1.f / (MAX_OXYGEN + 1) * (o2 + 1), 1.5f);
}

void OxygenIndicator::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!IsVisible() || GetSubsystem<Game>()->GetWorld().scene_ != eventData[SceneUpdate::P_SCENE].GetPtr())
        return;

    const int health{ GetSubsystem<HealthIndicator>()->GetHealth() };
    if (health > 0)
    {
        breath_ -= .23f * eventData[SceneUpdate::P_TIMESTEP].GetFloat();

        if (breath_ <= 0.f)
        {
            if (oxygen_ > 0)
            {
                SetOxygen(oxygen_ - 1);
            }
            else
            {
                SendEvent(E_PLAYERHIT);

                if (health == 1)
                    GetSubsystem<Game>()->GetWorld().tux_->Stun("");
            }

            breath_ = 1.f;
        }
    }

    const bool scuba{ GetSubsystem<Game>()->GetWorld().tux_->HasScuba() };
    bubble_->SetVisible(!scuba && (oxygen_ != 1 || breath_ > .9f || Mod(breath_, .05f) > .025f));
}

void OxygenIndicator::UpdateSizes()
{
    const float scale{ GUI::sizes_.screen_.y_ / 1080.f };
    constexpr int bubbleSize{ 96 };
    SetScale(scale);
    SetPosition(scale * bubbleSize, -GUI::sizes_.screen_.y_ + (scale * bubbleSize * 8/3));
}
