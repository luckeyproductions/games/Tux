/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../jukebox.h"
#include "gamemenu.h"
#include "../inputmaster.h"

GameMenu::GameMenu(Context* context): UIElement(context),
    firstElem_{ nullptr },
    lastElem_{ nullptr }
{
    SetAlignment(HA_CENTER, VA_TOP);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(GameMenu, HandleSizesChanged));
    SubscribeToEvent(E_MENUINPUT, DRY_HANDLER(GameMenu, HandleMenuInput));
}


Button* GameMenu::CreateTextButton(const String& text)
{
    SharedPtr<Button> button{ CreateChild<Button>(text + "Button") };

    button->SetAlignment(HA_CENTER, VA_TOP);
    button->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    Text* buttonText{ button->CreateChild<Text>() };
    buttonText->SetAlignment(HA_CENTER, VA_CENTER);
    buttonText->SetText(text);
    buttonText->SetFont(RES(Font, "Fonts/Ftoonk.otf"));
    buttonText->SetColor(Color::YELLOW.Lerp(Color::ORANGE, .25f));
    buttonText->SetTextEffect(TE_STROKE);
    buttonText->SetEffectRoundStroke(true);
    buttonText->SetEffectColor(Color::ORANGE.Lerp(Color::BLACK, .25f).Transparent(.42f));
    buttonText->SetOpacity(.8f);

//    if (text != "Exit")
    SubscribeToEvent(button, E_CLICK, DRY_HANDLER(GameMenu, HandleButtonClicked));

    return button;
}

void GameMenu::HandleMenuInput(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!IsVisible())
        return;

    UI* ui{ GetSubsystem<UI>() };
    UIElement* focusElem{ ui->GetFocusElement() };
    const VariantVector& varVec{ eventData[MenuInput::P_ACTIONS].GetVariantVector() };
    PODVector<MasterInputAction> actions{};
    for (const Variant& a: varVec)
        actions.Push(static_cast<MasterInputAction>(a.GetUInt()));

    const int step{ actions.Contains(MIA_DOWN)
                  - actions.Contains(MIA_UP) };
    const int side{ actions.Contains(MIA_RIGHT)
                  - actions.Contains(MIA_LEFT) };
    const int modify{ actions.Contains(MIA_INCREMENT)
                    - actions.Contains(MIA_DECREMENT) };

    if (!focusElem)
    {
        if (side + step > 0 || actions.Contains(MIA_CONFIRM))
            ui->SetFocusElement(firstElem_, true);
        else if (side + step < 0)
            ui->SetFocusElement(lastElem_, true);
    }
    else
    {
        //Change value
        if (modify)
        {
            for (UIElement* child: focusElem->GetChildren())
            {
                if (child->GetTypeInfo()->IsTypeOf(Slider::GetTypeStatic()))
                {
                    Slider* slider{ static_cast<Slider*>(child) };
                    slider->SetValue(slider->GetValue() + modify * .1f);
                }
                else if (child->GetTypeInfo()->IsTypeOf(DropDownList::GetTypeStatic()))
                {
                    DropDownList* dropDown{ static_cast<DropDownList*>(child) };
                    const unsigned index{ Clamp(dropDown->GetSelection() - modify, 0u, dropDown->GetNumItems() - 1) };
                    dropDown->SetSelection(index);
                    VariantMap selectedData{ { ItemSelected::P_ELEMENT, dropDown },
                                             { ItemSelected::P_SELECTION, index } };
                    dropDown->SendEvent(E_ITEMSELECTED, selectedData);
                }
            }
        }

        //Focus next element
        if (step > 0)
        {
            UIElement* nextElem{ static_cast<UIElement*>(focusElem->GetVar("Next").GetPtr()) };
            while (nextElem && !nextElem->IsEnabled() && nextElem != focusElem)
                nextElem = static_cast<UIElement*>(nextElem->GetVar("Next").GetPtr());

            ui->SetFocusElement(nextElem);
        }
        //Focus previous element
        if (step < 0)
        {
            UIElement* prevElem{ static_cast<UIElement*>(focusElem->GetVar("Prev").GetPtr()) };
            while (prevElem && !prevElem->IsEnabled() && prevElem != focusElem)
                prevElem = static_cast<UIElement*>(prevElem->GetVar("Prev").GetPtr());

            ui->SetFocusElement(prevElem);
        }
        //Focus right element
        if (side > 0)
        {
            UIElement* rightElem{ static_cast<UIElement*>(focusElem->GetVar("Right").GetPtr()) };
            while (rightElem && !rightElem->IsEnabled() && rightElem != focusElem)
                rightElem = static_cast<UIElement*>(rightElem->GetVar("Right").GetPtr());

            if (rightElem)
                ui->SetFocusElement(rightElem);
        }
        //Focus left element
        if (side < 0)
        {
            UIElement* leftElem{ static_cast<UIElement*>(focusElem->GetVar("Left").GetPtr()) };
            while (leftElem && !leftElem->IsEnabled() && leftElem != focusElem)
                leftElem = static_cast<UIElement*>(leftElem->GetVar("Left").GetPtr());

            if (leftElem)
                ui->SetFocusElement(leftElem);
        }
    }
}

bool GameMenu::Discard(VariantMap& eventData) const
{
    UIElement* element{ static_cast<UIElement*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    int mouseButton{ eventData[ClickEnd::P_BUTTON].GetInt() };

    if (!element->HasFocus() || mouseButton != MOUSEB_LEFT)
        return true;

    return false;
}

void GameMenu::HandleButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData) || static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr())->HasTag("Silent"))
        return;

    Node* musicNode{ GetSubsystem<JukeBox>()->GetMusicNode() };
    SoundSource* clickSource{ musicNode->CreateComponent<SoundSource>() };
    clickSource->SetAutoRemoveMode(REMOVE_COMPONENT);
    clickSource->Play(RES(Sound, "Samples/Click.wav"), 0.f, .3f);
}
