/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainmenu.h"
#include "sardinerunmenu.h"

#include "../settings.h"
#include "../game.h"

#include "settingsmenu.h"

SettingsMenu::SettingsMenu(Context* context): GameMenu(context),
    graphicsPanel_{ nullptr },
    audioPanel_   { nullptr },
    controlsPanel_{ nullptr },
    backButton_{ nullptr }
{
    SetVisible(false);
    Settings* settings{ GetSubsystem<Settings>() };

    GraphicsSettings* graphicsSettings{ settings->GetGraphicsSettings() };
    AudioSettings* audioSettings{ settings->GetAudioSettings() };

    graphicsPanel_ = AttributesToInputList(graphicsSettings);
    AddTitle(graphicsPanel_, "Graphics");

    Button* applyButton{ CreateTextButton("Apply") };
    applyButton->AddTag("Focus");
    applyButton->SetParent(graphicsPanel_);
    SubscribeToEvent(applyButton, E_CLICKEND, DRY_HANDLER(SettingsMenu, HandleApplyButtonClicked));

    audioPanel_ = AttributesToInputList(audioSettings);
    AddTitle(audioPanel_, "Audio");

    backButton_ = CreateTextButton("Back");
    SubscribeToEvent(backButton_, E_CLICKEND, DRY_HANDLER(SettingsMenu, HandleBackButtonClicked));

    lastElem_ = backButton_;

    const PODVector<UIElement*> elementSequence{ graphicsPanel_->GetChildrenWithTag("Focus", true) +
                                                 PODVector<UIElement*>{ backButton_ } +
                                                 audioPanel_->GetChildrenWithTag("Focus", true) };
    const unsigned backIndex{ elementSequence.IndexOf(backButton_) };

    for (unsigned e{ 0u }; e < elementSequence.Size(); ++e)
    {
        UIElement* elem{ elementSequence.At(e) };
        Vector<SharedPtr<UIElement> > children{ elem->GetChildren() };
        children.Push(SharedPtr<UIElement>{ elem });

        if (e == 0u)
        {
            firstElem_ = elem;
            for (UIElement* child: children)
                child->SetVar("Prev", elementSequence.Back());
        }
        else
        {
            for (UIElement* child: children)
                child->SetVar("Prev", elementSequence.At(e - 1));
        }

        if (e == elementSequence.Size() - 1)
        {
            for (UIElement* child: children)
                child->SetVar("Next", firstElem_);
        }
        else
        {
            for (UIElement* child: children)
                child->SetVar("Next", elementSequence.At(e + 1));
        }

        if (e < backIndex)
        {
            const unsigned sideIndex{ e + backIndex + 1 };
            UIElement* sideElem{ elementSequence.At(Min(sideIndex, elementSequence.Size() - 1)) };

            for (UIElement* child: children)
            {
                child->SetVar("Right", sideElem);
                child->SetVar("Left", sideElem);
            }

            if (sideIndex < elementSequence.Size())
            {
                Vector<SharedPtr<UIElement> > sideChildren{ sideElem->GetChildren() };
                sideChildren.Push(SharedPtr<UIElement>{ sideElem });

                for (UIElement* child: sideChildren)
                {
                    child->SetVar("Right", elem);
                    child->SetVar("Left", elem);
                }
            }
        }
    }

    UpdateSizes();
}

UIElement* SettingsMenu::AttributesToInputList(Serializable* serializable)
{
    const SettingsCategory category{ (!graphicsPanel_ ? SC_GRAPHICS :
                                     (!audioPanel_    ? SC_AUDIO
                                                      : SC_INPUT)) };

    const Vector<AttributeInfo>* attributes{ serializable->GetAttributes() };
    UIElement* panel{ CreateChild<Window>() };
    panel->SetAlignment(HA_CENTER, VA_TOP);
    panel->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    panel->SetOpacity(OCTAL(7));

    Window* settingRow{ nullptr };
    int sameRow{ 0 };

    const unsigned numAttributes{ attributes->Size() };
    for (unsigned a{ 0 }; a < numAttributes; ++a)
    {
        const AttributeInfo& info{ attributes->At(a) };
        if (!info.type_)
            continue;

        if (!settingRow || sameRow == 0)
        {
            settingRow = panel->CreateChild<Window>();
            settingRow->SetFocusMode(FM_FOCUSABLE);
            settingRow->SetAlignment(HA_CENTER, VA_TOP);
            settingRow->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
            settingRow->SetHoverOffset(16, 16);
            settingRow->AddTag("Focus");

            SubscribeToEvent(settingRow, E_CLICK, DRY_HANDLER(SettingsMenu, HandleRowClicked));
            SubscribeToEvent(settingRow, E_HOVERBEGIN, DRY_HANDLER(SettingsMenu, HandleRowHoverChanged));
            SubscribeToEvent(settingRow, E_HOVEREND, DRY_HANDLER(SettingsMenu, HandleRowHoverChanged));
        }

        if (sameRow == 0)
        {
            Text* attributeNameText{ settingRow->CreateChild<Text>() };
            String attributeText{ info.name_ };
            if (attributeText.Contains("Enabled"))
                attributeText = attributeText.Substring(0, info.name_.FindLast(' '));
            attributeNameText->SetText(attributeText);
            attributeNameText->SetFont(FONT_FTOONK);
            attributeNameText->SetColor(Color::ORANGE.Lerp(Color::YELLOW, 1/3.f));
            attributeNameText->SetAlignment(HA_LEFT, VA_CENTER);
        }

        sameRow = Max(0, sameRow - 1);

        UIElement* settingInput{ nullptr };
        switch (info.type_) {
        default: break;
        case VAR_BOOL:
        {
            CheckBox* checkBox{ settingRow->CreateChild<CheckBox>() };
            checkBox->SetChecked(serializable->GetAttribute(info.name_).GetBool());
            settingInput = checkBox;
            SubscribeToEvent(settingInput, E_TOGGLED, DRY_HANDLER(SettingsMenu, ModifySetting));
        }
        break;
        case VAR_FLOAT:
        {
            Slider* slider{ settingRow->CreateChild<Slider>() };
            slider->SetHoverOffset(16, 16);

            slider->SetRange(SLIDER_FACTOR);
            slider->SetValue(serializable->GetAttribute(info.name_).GetFloat() * SLIDER_FACTOR);

            settingInput = slider;
            SubscribeToEvent(settingInput, E_SLIDERCHANGED, DRY_HANDLER(SettingsMenu, ModifySetting));
        }
        break;
        case VAR_INTVECTOR2:
        {
            DropDownList* dropDownList{ settingRow->CreateChild<DropDownList>() };
            dropDownList->SetResizePopup(true);
            ListView* listView{ dropDownList->GetListView() };
            listView->SetScrollBarsVisible(false, true);

            if (info.name_ == GS_RESOLUTION)
                FillResolutionsList(dropDownList, serializable);

            settingInput = dropDownList;
            SubscribeToEvent(settingInput, E_ITEMSELECTED, DRY_HANDLER(SettingsMenu, ModifySetting));
        }
        break;
        }

        if (settingInput)
        {
            settingInput->SetVar("SettingsCategory", category);
            settingInput->SetVar("TargetAttributeName", info.name_);
            settingInput->SetAlignment(HA_RIGHT, VA_CENTER);
            settingInput->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
            settingInput->SetUseDerivedOpacity(false);

            if (info.type_ == VAR_BOOL)
            {
                settingInput->SetMinSize(12, 12);
                settingInput->SetMaxSize(M_MAX_INT, M_MAX_INT);
            }

            if (sameRow == 0)
            {
//                SubscribeToEvent(settingInput, E_HOVERBEGIN, DRY_HANDLER(SettingsMenu, HandleCheckBoxHoverChanged));
//                SubscribeToEvent(settingInput, E_HOVEREND, DRY_HANDLER(SettingsMenu, HandleCheckBoxHoverChanged));
            }

            SubscribeToEvent(settingInput, E_CLICK, DRY_HANDLER(GameMenu, HandleButtonClicked));
        }

        if (info.name_.Contains("Enabled"))
            sameRow = 1;
    }


    return panel;
}

void SettingsMenu::AddTitle(UIElement* panel, const String& text)
{
    UIElement* row{ panel->CreateChild<UIElement>("TitleRow", 0) };
    row->SetAlignment(HA_CENTER, VA_TOP);

    Text* titleText{ row->CreateChild<Text>() };
    String attributeText{ text };
    titleText->SetAlignment(HA_CENTER, VA_CENTER);
    titleText->SetText(attributeText);
    titleText->SetFont(RES(Font, "Fonts/Ftoonk.otf"));
    titleText->SetColor(Color::YELLOW.Lerp(Color::ORANGE, 1/3.f));
    titleText->SetTextEffect(TE_STROKE);
    titleText->SetEffectRoundStroke(true);
    titleText->SetEffectColor(Color::ORANGE.Lerp(Color::BLACK, .25f).Transparent(.42f));
}

void SettingsMenu::FillResolutionsList(DropDownList* dropDownList, Serializable* serializable)
{
    PODVector<IntVector3> freshrated{ GetSubsystem<Graphics>()->GetResolutions(0) };
    PODVector<IntVector2> noRefreshRate{};

    for (const IntVector3& resolution: freshrated)
    {
        const IntVector2 refreshrateless{ resolution.x_, resolution.y_ };
        if (!noRefreshRate.Contains(refreshrateless))
            noRefreshRate.Push(refreshrateless);
    }

    for (const IntVector2& resolution: noRefreshRate)
    {
        Text* text{ new Text(context_) };
        text->SetVar("Value", resolution);
        text->SetText(String{ resolution.x_ } + " x " + String{ resolution.y_ });
        text->SetFont(FONT_TOYS_TOSS);
        text->SetColor(Color::ORANGE);
        text->SetSelectionColor(Color::YELLOW.Transparent(.23f));
        dropDownList->AddItem(text);

        if (serializable->GetAttribute(GS_RESOLUTION).GetIntVector2() == resolution)
            dropDownList->SetSelectionAttr(dropDownList->GetNumItems() - 1);
    }
}

void SettingsMenu::ModifySetting(StringHash eventType, VariantMap& eventData)
{
    UIElement* settingsEdit{ static_cast<UIElement*>(eventData[Toggled::P_ELEMENT].GetPtr()) };
    const SettingsCategory category{ static_cast<SettingsCategory>(settingsEdit->GetVar("SettingsCategory").GetInt()) };
    const String attributeName{ settingsEdit->GetVar("TargetAttributeName") };

    Settings* settings{ GetSubsystem<Settings>() };
    Serializable* serializable{ nullptr };

    switch (category) {
    case SC_GRAPHICS: serializable = settings->GetGraphicsSettings(); break;
    case SC_AUDIO:    serializable = settings->GetAudioSettings(); break;
    default:
    break;
    }

    if (serializable)
    {
        const TypeInfo* type{ settingsEdit->GetTypeInfo() };

        if (type->IsTypeOf<CheckBox>())
        {
            CheckBox* checkBox{ static_cast<CheckBox*>(settingsEdit) };
            const bool value{ checkBox->IsChecked() };
            serializable->SetAttribute(attributeName, value);
        }
        else if (type->IsTypeOf<Slider>())
        {
            Slider* slider{ static_cast<Slider*>(settingsEdit) };
            const float value{ slider->GetValue() / SLIDER_FACTOR };

            serializable->SetAttribute(attributeName, value);
        }
        else if (type->IsTypeOf<DropDownList>())
        {
            DropDownList* dropDownList{ static_cast<DropDownList*>(settingsEdit) };
            const IntVector2 value{ dropDownList->GetSelectedItem()->GetVar("Value").GetIntVector2() };

            serializable->SetAttribute(attributeName, value);
        }
    }

    if (category == SC_GRAPHICS)
    {
        const bool immediate{ attributeName == GS_ANTIALIASING
                           || attributeName == GS_BLOOM
                           || attributeName == GS_SSAO };
        if (immediate)
            ApplyRenderPathSetting(attributeName);
    }
    else if (category == SC_AUDIO)
    {
        ApplyAudioSetting(attributeName);
    }
}

void SettingsMenu::ApplyRenderPathSetting(const String attributeName)
{
    GraphicsSettings* graphicsSettings{ GetSubsystem<Settings>()->GetGraphicsSettings() };
    Renderer* renderer{ GetSubsystem<Renderer>() };
    RenderPath* renderPath{ renderer->GetDefaultRenderPath() };

    if (attributeName == GS_ANTIALIASING)
        renderPath->SetEnabled("FXAA3", graphicsSettings->GetAttribute(GS_ANTIALIASING).GetBool());
    else if (attributeName == GS_BLOOM)
        renderPath->SetEnabled("BloomHDR", graphicsSettings->GetAttribute(GS_BLOOM).GetBool());
    else if (attributeName == GS_SSAO)
        renderPath->SetEnabled("SSAO", graphicsSettings->GetAttribute(GS_SSAO).GetBool());
}

void SettingsMenu::ApplyAudioSetting(const String attributeName)
{
    AudioSettings* audioSettings{ GetSubsystem<Settings>()->GetAudioSettings() };
    Audio* audio{ GetSubsystem<Audio>() };

    if (attributeName.Contains("Music"))
    {
        const float musicGain{ audioSettings->GetAttribute(AS_MUSIC_ENABLED).GetBool()
                             * audioSettings->GetAttribute(AS_MUSIC_GAIN).GetFloat() };

        audio->SetMasterGain(SOUND_MUSIC, musicGain);
    }
    else if (attributeName.Contains("Samples"))
    {
        const float effectGain{ audioSettings->GetAttribute(AS_SAMPLES_ENABLED).GetBool()
                              * audioSettings->GetAttribute(AS_SAMPLES_GAIN).GetFloat() };

        audio->SetMasterGain(SOUND_EFFECT, effectGain);
    }
}

void SettingsMenu::HandleRowClicked(StringHash eventType, VariantMap& eventData)
{
    HandleButtonClicked(eventType, eventData);

    UIElement* row{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };

    for (SharedPtr<UIElement> child: row->GetChildren() )
    {
        if (child->GetTypeInfo()->IsTypeOf(CheckBox::GetTypeInfoStatic()))
        {
            CheckBox* b{ static_cast<CheckBox*>(child.Get()) };
            b->SetChecked(!b->IsChecked());
            return;
        }
        if (child->GetTypeInfo()->IsTypeOf(DropDownList::GetTypeInfoStatic()))
        {
            DropDownList* d{ static_cast<DropDownList*>(child.Get()) };
            d->ShowPopup(!d->GetShowPopup());
            return;
        }
//        VariantMap eventData{};
//        eventData.Insert({ ClickEnd::P_ELEMENT, Variant{ lastChild } });
//        eventData.Insert({ ClickEnd::P_BUTTON, Variant{ MOUSEB_LEFT } });
//        lastChild->SendEvent(E_CLICK, eventData);
//        lastChild->SendEvent(E_CLICKEND, eventData);
    }
}

void SettingsMenu::HandleApplyButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GraphicsSettings* graphicsSettings{ GetSubsystem<Settings>()->GetGraphicsSettings() };
    const bool vSync{ graphicsSettings->GetAttribute(GS_VSYNC).GetBool() };
    const bool fullscreen{ graphicsSettings->GetAttribute(GS_FULLSCREEN).GetBool() };
    const IntVector2 resolution{ graphicsSettings->GetAttribute(GS_RESOLUTION).GetIntVector2() };

    Graphics* graphics{ GetSubsystem<Graphics>() };
    const bool tripleBuffer{ graphics->GetTripleBuffer() };
    const int multiSample{ graphics->GetMultiSample() };
    const int monitor{ graphics->GetMonitor() };
    const int refreshRate{ graphics->GetRefreshRate() };

    graphics->SetMode(resolution.x_, resolution.y_, fullscreen, false, true, false, vSync,
                      tripleBuffer, multiSample, monitor, refreshRate);
}

void SettingsMenu::Hide()
{
    SetVisible(false);

    Game* game{ GetSubsystem<Game>() };
    if (game)
    {
        GUI* gui{ GetSubsystem<GUI>() };
        UI* ui{ GetSubsystem<UI>() };
        if (game->GetStatus() == GS_MAIN)
        {
            MainMenu* mainMenu{ gui->GetMainMenu() };
            mainMenu->SetButtonsVisible(true);
            ui->SetFocusElement(mainMenu->SettingsButton());
        }
        else if (game->GetStatus() == GS_PAUSED)
        {
            SardineRunMenu* sardineMenu{ gui->GetSardineRunMenu() };
            sardineMenu->SetVisible(true);
            ui->SetFocusElement(sardineMenu->SettingsButton());
        }
    }
}

void SettingsMenu::HandleBackButtonClicked(StringHash eventType, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Hide();
}

void SettingsMenu::HandleCheckBoxHoverChanged(StringHash eventType, VariantMap& eventData)
{
//    UIElement* element{ static_cast<UIElement*>(eventData[HoverBegin::P_ELEMENT].GetPtr()) };

//    if (!element)
//        return;

//    BorderImage* parent{ static_cast<BorderImage*>(element->GetParent()) };
//    const int d{ element->IsHovering() * 16};
//    parent->SetImageRect(IntRect{ 48, 0, 64, 16 } + IntRect{ d, d, d, d });
}

void SettingsMenu::HandleRowHoverChanged(StringHash eventType, VariantMap& eventData)
{
//    UIElement* row{ static_cast<UIElement*>(eventData[HoverBegin::P_ELEMENT].GetPtr()) };

//    if (!row)
//        return;

//    BorderImage* lastChild{ static_cast<BorderImage*>(row->GetChild(row->GetNumChildren() - 1)) };
//    UI* ui{ GetSubsystem<UI>() };

//    if (row->IsHovering())
//        ui->SetFocusElement(lastChild);
//    else
//        ui->SetFocusElement(nullptr);
}

void SettingsMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    UpdateSizesOfPanel(graphicsPanel_);
    UpdateSizesOfPanel(audioPanel_);
//    UpdateSizesOfPanel(inputPanel_);

    backButton_->SetSize(GUI::VMax(23), GUI::VH(9));
    backButton_->SetPosition(0, GUI::sizes_.logo_ / 2 + GUI::VH(55));
    Text* backButtonText{ backButton_->GetChildStaticCast<Text>(0) };
    const float fontSize{ GUI::VH(5) };
    backButtonText->SetPosition(0, fontSize / 9);
    backButtonText->SetFontSize(fontSize);
    backButtonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
}

void SettingsMenu::UpdateSizesOfPanel(UIElement* panel)
{
    const int side{ (panel == audioPanel_) - (panel == graphicsPanel_) };
    const int panelWidth{ RoundToInt((GUI::VMin(48) + GUI::VW(48)) * .5f) };
    const int rowHeight{ RoundToInt(GUI::VH(6)) };
    const int sideSpace{ Min(rowHeight / 2, GUI::VW(2)) };
    const int rowWidth{ panelWidth - sideSpace };
    const int panelY{ (rowHeight + GUI::sizes_.logo_) / 2 };
    panel->SetSize(panelWidth, GUI::VH(55));
    panel->SetPosition((panelWidth + sideSpace) * .5f * side, panelY);

    for (unsigned r{ 0 }; r < panel->GetNumChildren(); ++r)
    {
        UIElement* row{ panel->GetChild(r) };

        if (row->GetName() == "ApplyButton")
            row->SetSize(rowWidth / 3, rowHeight);
        else
            row->SetSize(rowWidth, rowHeight);

        row->SetPosition(0, (r + .2f) * (rowHeight + 4) + (rowHeight / 4) * (r > 3));

        const float fontSize{ GUI::VMin(3) };

        for (unsigned i{ 0 }; i < row->GetNumChildren(); ++i)
        {
            UIElement* childElement{ row->GetChild(i)};
            childElement->SetHeight(rowHeight - 8);

            const TypeInfo* type{ childElement->GetTypeInfo() };

            if (type->IsTypeOf<Text>())
            {
                Text* text{ static_cast<Text*>(childElement) };
                text->SetPosition((text->GetHorizontalAlignment() == HA_CENTER ? 0 : rowHeight / 6), fontSize / 9);
                text->SetFontSize(fontSize);
                text->SetEffectStrokeThickness(Ceil(fontSize / 23));
            }
            else if (type->IsTypeOf<CheckBox>())
            {
                childElement->SetHeight(rowHeight - 16);
                childElement->SetWidth(rowHeight - 16);
                childElement->SetPosition(-8, 0);
            }
            else if (type->IsTypeOf<Slider>())
            {
                childElement->SetWidth(Min(panelWidth - fontSize * 7 - rowHeight, rowHeight * 7));
                childElement->SetPosition(-rowHeight, 0);
            }
            else if (type->IsTypeOf<DropDownList>())
            {
                childElement->SetWidth(Min(panelWidth - fontSize * 9, rowHeight * 7));
                childElement->SetPosition(-4, 0);

                DropDownList* ddl{ static_cast<DropDownList*>(childElement) };
                for (UIElement* item: ddl->GetItems())
                {
                    Text* text{ static_cast<Text*>(item) };
                    text->SetFontSize(fontSize * OCTAL(6));
                    text->SetFixedHeight(fontSize);
                }
                ListView* listView{ ddl->GetListView() };
                listView->SetFixedHeight(GUI::VH(34));
                if (ddl->GetShowPopup())
                    ddl->ShowPopup(true);
            }
        }
    }
}
