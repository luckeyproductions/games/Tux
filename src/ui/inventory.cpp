/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gui.h"
#include "../game.h"

#include "inventory.h"

void Inventory::RegisterObject(Context* context)
{
    context->RegisterFactory<Inventory>();

    DRY_COPY_BASE_ATTRIBUTES(UIElement);
}

Inventory::Inventory(Context* context): UIElement(context),
    currentIndex_{ 0 },
    selector_{ nullptr },
    icons_{},
    items_{}
{
    context_->RegisterSubsystem(this);

    SetAlignment(HA_RIGHT, VA_TOP);
    SetOpacity(.3f);

    selector_ = CreateChild<BorderImage>("ItemSelector");
    selector_->SetTexture(RES(Texture2D, "UI/Selector.png"));
    selector_->SetAlignment(HA_CENTER, VA_CENTER);
    selector_->SetBlendMode(BLEND_ALPHA);

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Inventory, HandleSizesChanged));

    UpdateSizes();
}

void Inventory::Reset()
{
    items_.Clear();
    currentIndex_ = -1;
    UpdateIcons();
}

void Inventory::AddItem(const String& itemName)
{
    if (itemName.IsEmpty())
        return;

    if (items_.Contains(itemName))
        items_.Insert(items_.IndexOf(itemName), itemName);
    else
        items_.Push(itemName);

    if (currentIndex_ == -1)
        currentIndex_ = 0;

    UpdateIcons();
}

bool Inventory::RemoveItem(const String& itemName)
{
    if (!itemName.IsEmpty() && items_.Remove(itemName))
    {
        if (items_.IsEmpty())
            currentIndex_ = -1;
        else
            currentIndex_ = Clamp(currentIndex_, 0, static_cast<int>(GetUnique().Size()) - 1);

        UpdateIcons();
        return true;
    }

    return false;
}

String Inventory::GetSelected() const
{
    return GetUnique().At(currentIndex_);
}

void Inventory::UseSelected()
{
    if (currentIndex_ < 0)
        return;

    Tux* tux{ GetSubsystem<Game>()->GetWorld().tux_ };
    const String currentItem{ GetSelected() };
    const bool singleUse{ currentItem != "Bullwhip" && currentItem != "Tools" && !currentItem.Contains("Spell")};
    if (tux->CanUse(currentItem) && (!singleUse || RemoveItem(currentItem)))
        tux->UseItem(currentItem);
}

void Inventory::Step(int step)
{
    if (icons_.IsEmpty())
        return;

    int index{ currentIndex_ - step };
    while (index < 0)
        index += icons_.Size();
    if (index >= static_cast<int>(icons_.Size()))
        index %= icons_.Size();

    Select(index);
}

void Inventory::RemoveHatDependentItems(const String& hatModelName)
{
    if (hatModelName.Contains("Fedora"))
    {
        RemoveItem("Bullwhip");
    }
    else if (hatModelName.Contains("HardHat"))
    {
        while (ContainsItem("Bricks"))
            RemoveItem("Bricks");

        RemoveItem("Tools");
    }
    else if (hatModelName == "WizardHat")
    {
        RemoveItem("FireSpell");
        RemoveItem("IceSpell");
    }
}

void Inventory::Select(int index)
{
    currentIndex_ = Clamp(index, -1, static_cast<int>(icons_.Size() - 1));
    UpdateSizes();
}

void Inventory::UpdateIcons()
{
    SetOpacity(.3f + .7f * !items_.IsEmpty());

    for (Sprite* s: icons_)
        s->Remove();
    icons_.Clear();

    for (const String& uniqueName: GetUnique())
    {
        Sprite* icon{ CreateChild<Sprite>() };
        icon->SetSize(112, 112);
        icon->SetTexture(RES(Texture2D, "UI/" + uniqueName + ".png"));
        icon->SetAlignment(HA_LEFT, VA_CENTER);
        icon->SetBlendMode(BLEND_ALPHA);
        icon->SetPivot(.5f, .5f);
        icons_.Push(icon);

        int count{ 0 };
        for (const String& item: items_)
        {
            if (item == uniqueName)
                ++count;
        }

        if (count > 1)
        {
            Text* counter{ icon->CreateChild<Text>() };
            counter->AddTag("Counter");
            counter->SetAlignment(HA_LEFT, VA_TOP);
            counter->SetPivot(1.f, 1.f);
            counter->SetFont(FONT_FTOONK);
            counter->SetColor(Color::YELLOW.Lerp(Color::ORANGE, .5f));
            counter->SetTextEffect(TE_STROKE);
            counter->SetEffectRoundStroke(true);
            counter->SetEffectColor(Color::ORANGE.Lerp(Color::BLACK, .5f));
            counter->SetUseDerivedOpacity(false);
            counter->SetText(String{ count });
        }
    }

    UpdateSizes();
}

void Inventory::UpdateSizes()
{
    const float scale{ GUI::sizes_.screen_.y_ / 1080.f };
    SetPosition(-64 * scale, 224 * scale - GUI::sizes_.screen_.y_);

    const int selectorSize{ static_cast<int>(160 * scale) };
    selector_->SetSize(selectorSize, selectorSize);
    selector_->SetPosition((-Max(0, currentIndex_) * 160) * scale, 0);

    for (int s{ static_cast<int>(icons_.Size()) - 1 }; s >= 0; --s)
    {
        Sprite* icon{ icons_.At(s) };
        icon->SetScale(scale);
        icon->SetPosition((-s * 160) * scale - 56 * scale, -56 * scale);
    }

    PODVector<UIElement*> counters{};
    GetChildrenWithTag(counters, "Counter", true);
    for (UIElement* elem: counters)
    {
        Text* counter{ static_cast<Text*>(elem) };
        const int fontSize{ static_cast<int>(scale * GUI::sizes_.text_[2]) };
        counter->SetFontSize(fontSize);
        counter->SetPosition(28 * scale, 124 * scale);
        counter->SetEffectStrokeThickness(CeilToInt(scale * 1.75f));
    }
}

StringVector Inventory::GetUnique() const
{
    StringVector unique{};
    for (const String& i: items_)
    {
        if (!unique.Contains(i))
            unique.Push(i);
    }

    return unique;
}
