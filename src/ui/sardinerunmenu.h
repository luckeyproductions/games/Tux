/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SARDINERUNMENU_H
#define SARDINERUNMENU_H

#include "gamemenu.h"

enum SardineRunButton{ SRMB_CONTINUE = 0, SRMB_RESTART, SRMB_LEVEL, SRMB_SETTINGS, SRMB_MAIN, SRMB_START, SRMB_BACK, SRMB_ALL };

class SardineRunMenu: public GameMenu
{
    DRY_OBJECT(SardineRunMenu, GameMenu);

    const StringVector buttonsText_
    {
        { "Continue"  },
        { "Restart"   },
        { "Level"     },
        { "Settings"  },
        { "Main Menu" },
        { "Start"     },
        { "Back"      }
    };

public:
    SardineRunMenu(Context* context);

    void Hide() override;
    void SetLevelPickerVisible(bool visible);
    Button* SettingsButton() const { return *buttons_[SRMB_SETTINGS]; }
    void UpdateScoreList(const String& level);

protected:
    void UpdateSizes() override;
    void HandleMenuInput(StringHash eventType, VariantMap& eventData) override;

private:
    void CreatePanels();
    void CreateButtons();
    void SetActivePanel(unsigned index);
    void SetPage(unsigned page);
    Window* GetPanelFromLevel(const String& level);

    void MarkupScoreText(Text* text)
    {
        text->SetAlignment(HA_RIGHT, VA_TOP);
        text->SetFont(FONT_TOYS_TOSS);
        text->SetColor(Color::ORANGE);
    }

    void HandlePanelClicked(StringHash eventType, VariantMap& eventData);
    void HandleArrowClicked(StringHash eventType, VariantMap& eventData);
    void HandleStartButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleBackButtonClicked(StringHash eventType, VariantMap& eventData);

    void HandleContinueButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleRestartButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleLevelButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleSettingsButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleMainMenuButtonClicked(StringHash eventType, VariantMap& eventData);

    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);

    HashMap<int, Button*> buttons_;
    Vector<Button*> arrows_;
    Vector<Window*> panels_;
    unsigned cells_;
    unsigned page_;
    unsigned lastPage_;
    unsigned activePanel_;
};

#endif // SARDINERUNMENU_H
