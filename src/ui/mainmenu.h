/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINMENU_H
#define MAINMENU_H

#include "gamemenu.h"

enum MainMenuButton{ MMB_STORY = 0, MMB_SARDINE, MMB_SETTINGS, MMB_EXIT, MMB_ALL };

class MainMenu: public GameMenu
{
    const StringVector buttonsText_
    {
        { "Story Mode"  },
        { "Sardine Run" },
        { "Settings"    },
        { "Exit"        }
    };

    DRY_OBJECT(MainMenu, GameMenu);

public:
    MainMenu(Context* context);

    void SetButtonsVisible(bool visible);
    Button* SettingsButton() const { return *buttons_[MMB_SETTINGS]; }

protected:
    void HandleMenuInput(StringHash eventType, VariantMap& eventData) override;
    void UpdateSizes() override;

private:
    void HandleSardineRunButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleSettingsButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleExitButtonClicked(StringHash eventType, VariantMap& eventData);

    Sprite* tux_;
    Sprite* logo_;
    HashMap<int, Button*> buttons_;
};

#endif // MAINMENU_H
