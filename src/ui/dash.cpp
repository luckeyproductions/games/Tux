/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../scores.h"
#include "../items/sardine.h"
#include "../spawnmaster.h"

#include "dash.h"

Dash::Dash(Context* context): UIElement(context),
    healthIndicator_{ nullptr },
    sardineCounter_{ nullptr },
    counting_{ true },
    time_{ 0.f }
{
    SetAlignment(HA_CENTER, VA_BOTTOM);
    SetVisible(false);

    HealthIndicator::RegisterObject(context);
    CreateChild<HealthIndicator>();
    OxygenIndicator::RegisterObject(context);
    CreateChild<OxygenIndicator>();
    Inventory::RegisterObject(context);
    CreateChild<Inventory>();

    CreateSardineCounter();
    CreateTimer();

    UpdateSizes();

    SubscribeToEvent(E_UISIZESCHANGED, DRY_HANDLER(Dash, HandleSizesChanged));
    SubscribeToEvent(E_LASTSARDINE, DRY_HANDLER(Dash, HandleLastSardine));
}

void Dash::StopTime()
{
    counting_ = false;
}

void Dash::HandleLastSardine(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Scores* scores{ GetSubsystem<Scores>() };
    const String level{ context_->GetGlobalVar("Level").ToString() };
    scores->AddScore(level, Floor(time_ * 1000));

    StopTime();
}

void Dash::UpdateSardineCount()
{
    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>()};
    SetSardineCount(spawn->CountActive<Sardine>());
}

void Dash::SetSardineCount(int sardines)
{
    Text* number{ sardineCounter_->GetChildStaticCast<Text>(String{ "Number" }) };
    number->SetText(String{ sardines });
}

void Dash::UpdateTimer()
{
    Text* timer{ GetChildStaticCast<Text>(String{ "Timer" }) };

    timer->SetText(GUI::SecondsToTimeCode(time_));
}

void Dash::SetTimerScene(Scene* scene, bool reset)
{
    UnsubscribeFromEvent(E_SCENEUPDATE);
    timerScene_ = scene;

    if (reset)
    {
        counting_ = true;
        time_ = 0.f;
        UpdateTimer();
    }

    SubscribeToEvent(scene, E_SCENEUPDATE, DRY_HANDLER(Dash, HandleSceneUpdate));
}

void Dash::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const float timeStep{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };

    if (timeStep != 0.f && counting_)
    {
        time_ += timeStep;
        UpdateTimer();
    }
}

void Dash::CreateSardineCounter()
{
    sardineCounter_ = CreateChild<UIElement>();

    Sprite* sardineIcon{ sardineCounter_->CreateChild<Sprite>("Icon") };
    sardineIcon->SetAlignment(HA_LEFT, VA_CENTER);
    sardineIcon->SetTexture(RES(Texture2D, "UI/Sardine.png"));
    sardineIcon->SetBlendMode(BLEND_ALPHA);
    Text* number{ sardineCounter_->CreateChild<Text>("Number") };
    number->SetAlignment(HA_LEFT, VA_CENTER);
    number->SetFont(FONT_BOLEH);
    number->SetColor(Color::YELLOW.Lerp(Color::ORANGE, .5f));
    number->SetTextEffect(TE_STROKE);
    number->SetEffectRoundStroke(true);
    number->SetEffectColor(Color::ORANGE.Lerp(Color::BLACK, .25f).Transparent(.42f));
    number->SetOpacity(.8f);
}

void Dash::CreateTimer()
{
    Text* timer{ CreateChild<Text>("Timer") };
    timer->SetAlignment(HA_RIGHT, VA_CENTER);
    timer->SetFont(FONT_BOLEH);
    timer->SetColor(Color::YELLOW.Lerp(Color::ORANGE, .5f));
    timer->SetTextEffect(TE_STROKE);
    timer->SetEffectRoundStroke(true);
    timer->SetEffectColor(Color::ORANGE.Lerp(Color::BLACK, .25f).Transparent(.42f));
    timer->SetOpacity(.8f);

    UpdateTimer();
}

void Dash::UpdateSizes()
{
    const int iconSize{ static_cast<int>(GUI::VH(8)) };
    const int fontSize{ iconSize * 3/4 };

    sardineCounter_->GetChild(String{ "Icon" })->SetSize(iconSize, iconSize);

    Text* number{ sardineCounter_->GetChildStaticCast<Text>(String{ "Number" }) };
    number->SetSize(iconSize * 4, iconSize);
    number->SetFontSize(fontSize);
    number->SetPosition(iconSize * 1.17f, fontSize * 2/3.f);
    number->SetEffectStrokeThickness(1 + Ceil(iconSize / 23.f));

    Text* timer{ GetChildStaticCast<Text>(String{ "Timer" }) };
    timer->SetSize(iconSize * 4, iconSize);
    timer->SetFontSize(fontSize);
//    timer->SetPosition(iconSize * 1.17f, fontSize * 2/3.f);
    timer->SetEffectStrokeThickness(1 + Ceil(iconSize / 23.f));
    timer->SetPosition(0, 0);

    SetSize(GUI::VW(95), iconSize);
    SetPosition(0, -GUI::VMin(4));
}
