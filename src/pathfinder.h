/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "mastercontrol.h"

class PathFinder: public Component
{
    DRY_OBJECT(PathFinder, Component);

    struct NodeData
    {
        float fromStart_{ M_INFINITY };
        IntVector3 previous_{ M_MAX_INT, M_MAX_INT, M_MAX_INT };
        float penalty_{ 1.f };
    };

    class Graph: public HashMap<IntVector3, NodeData>
    {
        public:
            bool Add(const IntVector3& coordinate)
            {
                if (Contains(coordinate))
                    return false;

                Insert({ coordinate, NodeData{} });
                return true;
            }
            void Add(const HashSet<IntVector3>& coordinates)
            {
                for (const IntVector3& coordinate: coordinates)
                    Insert({ coordinate, NodeData{} });
            }

            HashSet<IntVector3> Neighbors(const IntVector3& coord, bool onlyOrth = false) const;
            IntVector3 Nearest(const HashSet<IntVector3>& frontier, const IntVector3& goal) const;
    };

public:
    PathFinder(Context* context);

    Vector<IntVector3> FindPathDiagonal(const Vector3& start, IntVector3 to, bool underwater) const;
    Vector<IntVector3> FindPathOrthogonal(const Vector3& start, IntVector3 to, bool underwater) const;

private:
    IntVector3 Nearest(const Vector<IntVector3>& nodes, const IntVector3& pos) const;
    Vector<IntVector3> Reachable(const Vector<IntVector3>& nodes, const IntVector3& from) const;
};

static HashSet<IntVector3> Neighbors(const IntVector3& coords, bool diagonal = false, bool vertical = true)
{
    HashSet<IntVector3> neighbors{};

    for (int dx{ -1 }; dx <= 1; ++dx)
    for (int dy{ -1 }; dy <= 1; ++dy)
    for (int dz{ -1 }; dz <= 1; ++dz)
    {
        if (!vertical && dy != 0)
            continue;

        const int zero{ (dx == 0) + (dy == 0) + (dz == 0) };
        if (zero == 3 || diagonal == (zero == 2))
            continue;

        neighbors.Insert(coords + IntVector3{ dx, dy, dz });
    }

    return neighbors;
}

static HashSet<IntVector3> AllNeighbors(const IntVector3& coords, bool vertical = true)
{
    HashSet<IntVector3> neighbors{ Neighbors(coords, true, vertical) };
    neighbors.Insert(Neighbors(coords, false, vertical));

    return neighbors;
}

#endif // PATHFINDER_H
