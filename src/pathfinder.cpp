/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "blockmap/blockmap.h"

#include "pathfinder.h"

PathFinder::PathFinder(Context* context): Component(context)
{
}

Vector<IntVector3> PathFinder::FindPathDiagonal(const Vector3& start, IntVector3 to, bool underwater) const
{
    BlockMap* blockMap{ GetSubsystem<BlockMap>() };
    Graph graph{};
    graph.Add(blockMap->GetUnoccupied(underwater));

    Vector<IntVector3> nodes{ graph.Keys() };
    if (nodes.IsEmpty())
        return {};

    IntVector3 from{ VectorRoundToInt(start) };

    if (!nodes.Contains(from))
        from = Nearest(nodes, from);

    nodes = Reachable(nodes, from);

    if (!nodes.Contains(to))
        to = Nearest(nodes, to);

    HashSet<IntVector3> frontier{ from };
    graph[from].fromStart_ = 0.f;

    while (!frontier.IsEmpty())
    {
        const IntVector3 current{ graph.Nearest(frontier, to) };

        // Found path
        if (current == to)
        {
            Vector<IntVector3> path{ to };
            while(graph[path.Back()].previous_.x_ != M_MAX_INT)
                path.Push(graph[path.Back()].previous_);

            return path;
        }

        // Continue search
        const HashSet<IntVector3> neighbors{ graph.Neighbors(current) };
        for (const IntVector3& neighbor: neighbors)
        {
            const float neighborDistance{ Vector3{ current }.DistanceToPoint(neighbor) };
            const float fromStart{ graph[current].fromStart_ + neighborDistance * graph[neighbor].penalty_ };
            if (fromStart < graph[neighbor].fromStart_)
            {
                graph[neighbor].fromStart_ = fromStart;
                graph[neighbor].previous_ = current;

                frontier.Insert(neighbor);
            }
        }

        frontier.Erase(current);
    }

    return {};
}

Vector<IntVector3> PathFinder::FindPathOrthogonal(const Vector3& start, IntVector3 to, bool underwater) const
{
    BlockMap* blockMap{ GetSubsystem<BlockMap>() };
    Graph graph{};
    graph.Add(blockMap->GetUnoccupied(underwater));

    Vector<IntVector3> nodes{ graph.Keys() };
    if (nodes.IsEmpty())
        return {};

    IntVector3 from{ VectorRoundToInt(start) };

    if (!nodes.Contains(from))
        from = Nearest(nodes, from);

    nodes = Reachable(nodes, from);

    if (!nodes.Contains(to))
        to = Nearest(nodes, to);

    HashSet<IntVector3> frontier{ from };
    graph[from].fromStart_ = 0.f;

    while (!frontier.IsEmpty())
    {
        const IntVector3 current{ graph.Nearest(frontier, to) };
        // Found path
        if (current == to)
        {
            Vector<IntVector3> path{ to };

            while (graph[path.Back()].previous_.x_ != M_MAX_INT)
                path.Push(graph[path.Back()].previous_);

            return path;
        }
        // Continue search
        const HashSet<IntVector3> neighbors{ graph.Neighbors(current, true) };
        for (const IntVector3& neighbor: neighbors)
        {
            const float neighborDistance{ Vector3{ current }.DistanceToPoint(neighbor) };
            const float fromStart{ graph[current].fromStart_ + neighborDistance * graph[neighbor].penalty_ };
            if (fromStart < graph[neighbor].fromStart_)
            {
                graph[neighbor].fromStart_ = fromStart;
                graph[neighbor].previous_ = current;

                frontier.Insert(neighbor);
            }
        }

        frontier.Erase(current);
    }

    return {};
}

IntVector3 PathFinder::Nearest(const Vector<IntVector3>& nodes, const IntVector3& pos) const
{
    IntVector3 closestToStart{ M_MAX_INT, M_MAX_INT, M_MAX_INT };
    float distance{ M_INFINITY };

    for (const IntVector3& coords: nodes)
    {
        const float d{ Vector3{ coords }.DistanceToPoint(pos) };
        if (d < distance)
        {
            closestToStart = coords;
            distance = d;

            if (distance < M_1_SQRT2)
                break;
        }
    }
    return closestToStart;
}

Vector<IntVector3> PathFinder::Reachable(const Vector<IntVector3>& nodes, const IntVector3& from) const
{
    if (!nodes.Contains(from))
        return {};

    Vector<IntVector3> reachable{};
    HashSet<IntVector3> frontier{ from };

    while (!frontier.IsEmpty())
    {
        const IntVector3 front{ frontier.Front() };
        for (const IntVector3& n: AllNeighbors(front))
        {
            if (nodes.Contains(n) && !reachable.Contains(n) && !frontier.Contains(n))
                frontier.Insert(n);
        }

        reachable.Push(front);
        frontier.Erase(front);
    }

    return reachable;
}

HashSet<IntVector3> PathFinder::Graph::Neighbors(const IntVector3& coord, bool onlyOrth) const
{
    const Vector<IntVector3> coords{ Keys() };
    HashSet<IntVector3> nigh{};

    for (int dx{ -1 }; dx <= 1; ++dx)
    for (int dy{ -1 }; dy <= 1; ++dy)
    for (int dz{ -1 }; dz <= 1; ++dz)
    {
        const int zero{ (dx == 0) + (dy == 0) + (dz == 0) };
        if (zero == 3 || (onlyOrth && (zero != 2)))
            continue;

        if (Abs(dx) == Abs(dy) && Abs(dx) == Abs(dz))
        {
            const IntVector3 relevantX{ IntVector3{ dx,  0,  0 } + coord };
            const IntVector3 relevantY{ IntVector3{  0, dy,  0 } + coord };
            const IntVector3 relevantZ{ IntVector3{  0,  0, dz } + coord };
            if (!coords.Contains(relevantX) || !coords.Contains(relevantY) || !coords.Contains(relevantZ))
                continue;
        }

        const IntVector3 neighbor{ IntVector3{ dx, dy, dz } + coord };
        if (coords.Contains(neighbor))
            nigh.Insert(neighbor);
    }

    return nigh;
}

IntVector3 PathFinder::Graph::Nearest(const HashSet<IntVector3>& frontier, const IntVector3& goal) const
{
    float lowest{ M_INFINITY };
    IntVector3 nearest{ M_MAX_INT, M_MAX_INT, M_MAX_INT };

    for (const IntVector3& coord: frontier)
    {

        const NodeData& node{ *operator [](coord) };
        const float toGoal{ Vector3{ coord }.DistanceToPoint(goal) };

        if (node.fromStart_ + toGoal < lowest)
        {
            nearest = coord;
            lowest = node.fromStart_ + toGoal;
        }
    }

    return nearest;
}
