/* Edddy
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockmap.h"
#include "cyberplasm/cyberplasm.h"
#include "../items/hardhat.h"

#include "tile.h"

Tile::Tile(Context* context): Block(context),
    tileModel_{ nullptr },
    profile_{}
{
}

void Tile::Initialize(const HeightProfile& profile, Material* material, bool collider)
{
    profile_ = profile;
    generateTile();
    tileModel_->SetMaterial(material);

    if (collider)
    {
        collider_ = node_->GetParent()->CreateComponent<CollisionShape>();
        collider_->SetMargin(.025f);

        if (!profile_.isFlat())
        {
            collider_->SetTriangleMesh(tileModel_->GetModel(), 0, Vector3::ONE, Vector3::DOWN * collider_->GetMargin() + node_->GetPosition(), node_->GetRotation());
            collider_->SetLodLevel(2u);
        }
        else
        {
            const Vector3 colliderPosition{ node_->GetPosition() + Vector3::DOWN * collider_->GetMargin() + Vector3::UP * (profile_.min() - 1) + profile_.offCenter() };
            collider_->SetBox({ 1.f * profile_.size().x_, 1.f, 1.f * profile_.size().y_ }, colliderPosition);

            const IntRect& span{ profile_.span() };
            for (int x{ span.Min().x_ }; x < span.Max().x_; ++x)
            for (int z{ span.Min().y_ }; z < span.Max().y_; ++z)
            {
                const IntVector3 offset{ x, profile_.min() - 1, z };
                HardHat::AddFoundation(VectorRoundToInt(node_->GetWorldPosition() + offset));
            }
        }
    }
}

void Tile::OnNodeSet(Node *node)
{
    if (!node)
        return;

    tileModel_ = node_->CreateComponent<StaticModel>();
    tileModel_->SetCastShadows(true);
}

void Tile::generateTile()
{
    BlockMap* blockMap{ node_->GetParent()->GetComponent<BlockMap>() };

    if (!blockMap)
        return;

    Witch::Spell rect{ {}, Witch::Spell::QUAD };
    for (unsigned c{ 0u }; c < 4u; ++c)
        rect.Push(cornerRune(c, false));

    Cyberplasm* cyberplasm{ GetSubsystem<Cyberplasm>() };
    SharedPtr<Model> tileModel{ context_->CreateObject<Model>() };

    if (profile_.isFlat())
    {
        tileModel = cyberplasm->Summon({ rect }, 0);
    }
    else
    {
        const IntVector2 size{ profile_.size() };
        Vector3 boundsMin{  Vector3::ONE * M_INFINITY};
        Vector3 boundsMax{ -Vector3::ONE * M_INFINITY};

        const int lods{ 5 };
        tileModel->SetNumGeometries(1);
        tileModel->SetNumGeometryLodLevels(0, lods);

        for (int lod{ 0 }; lod < lods; ++lod)
        {
            const int r{ Max(1, 1 << (lods - lod - 1)) };
            const Witch::Form form{ rect.Cast({ r * size.x_ - 1, r * size.y_ - 1 }) };
            tileModel->SetGeometry(0, lod, cyberplasm->Conjure(form));
            tileModel->GetGeometry(0, lod)->SetLodDistance(lod * 23.5f);

            boundsMax = VectorMax(boundsMax, form.bounds_.min_);
            boundsMin = VectorMin(boundsMin, form.bounds_.max_);
        }

        tileModel->SetBoundingBox({ boundsMin, boundsMax });
    }

    tileModel_->SetModel(tileModel);
}

Witch::Rune Tile::cornerRune(int index, bool global) const
{
    BlockMap* blockMap{ node_->GetParent()->GetComponent<BlockMap>() };

    if (!blockMap)
        return {};

    const IntVector2 size{ profile_.size() };
    const Vector3 blockSize{ blockMap->GetBlockSize() };
    const Vector3 offCenter{ profile_.offCenter() * blockMap->GetBlockSize() };
    const Vector3 halfSize{ blockSize.x_ * size.x_ * .5f, blockSize.y_ * .5f, blockSize.z_ * size.y_ * .5f };
    const float blockHeight{ blockSize.y_ };

    const int e{ profile_.elevation().At(index) };
    const IntVector3 offset{ HeightProfile::cornerOffset(index) * 2 - IntVector3{ 1, 0, 1 } };
    const Vector3 pos{ halfSize * offset + Vector3::UP * (e * blockHeight - halfSize.y_) + offCenter };
    const Vector3 normal{ profile_.normal(index) };

    Witch::Rune rune{ pos, normal };

    if (global)
    {
        return rune.Transformed(node_->GetWorldTransform());
    }
    else
    {
        const Quaternion rot{ node_->GetRotation() };
        return rune.Transformed(Matrix3x4{ {}, rot.Inverse(), node_->GetScale() });
    }
}
