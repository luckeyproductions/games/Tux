/* MIT License
//
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
*/

#include <Dry/Graphics/IndexBuffer.h>

#include "cyberplasm.h"

Cyberplasm::Cyberplasm(Context* context): Object(context)
{
}

using namespace Witch;

SharedPtr<Geometry> Cyberplasm::Conjure(const Form& form) const
{
    const PODVector<float>& vertexData{ form.vertexData_ };
    const PODVector<unsigned short>& indexData{ form.indexData_ };

    const unsigned numVertices{ indexData.Size() };

    SharedPtr<VertexBuffer> vb{ new VertexBuffer{ context_ } };
    SharedPtr<IndexBuffer> ib{ new IndexBuffer{ context_ } };

    // We could use the "legacy" element bitmask to define elements for more compact code, but let's demonstrate
    // defining the vertex elements explicitly to allow any element types and order
    PODVector<VertexElement> elements;
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_POSITION });
    elements.Push(VertexElement{ TYPE_VECTOR3, SEM_NORMAL });

    vb->SetSize(numVertices, elements);
    vb->SetShadowed(true);

    ib->SetSize(numVertices, false);
    ib->SetShadowed(true);

    vb->Lock(0, numVertices);
    vb->SetData(vertexData.Buffer());

    ib->Lock(0, numVertices);
    ib->SetData(indexData.Buffer());

    vb->Unlock();
    ib->Unlock();

    SharedPtr<Geometry> vat{ new Geometry{ context_ } };
    vat->SetVertexBuffer(0, vb);
    vat->SetIndexBuffer(ib);
    vat->SetDrawRange(form.type_, 0, numVertices);

    return vat;
}

SharedPtr<Model> Cyberplasm::Summon(const Sect& sect, unsigned resolution) const
{
    SharedPtr<Model> model{ context_->CreateObject<Model>() };

    BoundingBox bb{};
    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        for (unsigned t{ 0 }; t < sect.At(s).Size(); ++t)
            bb.Merge(sect.At(s).At(t).Position());
    }

    model->SetNumGeometries(sect.Size());
    Vector<SharedPtr<VertexBuffer> > vertexBuffers;
    Vector<SharedPtr<IndexBuffer> > indexBuffers;

    for (unsigned s{ 0u }; s < sect.Size(); ++s)
    {
        SharedPtr<Geometry> g{ Conjure(sect.At(s).Cast(resolution)) };
        model->SetGeometry(s, 0, g);
        vertexBuffers.Push(SharedPtr<VertexBuffer>(g->GetVertexBuffer(0)));
        indexBuffers.Push(SharedPtr<IndexBuffer>(g->GetIndexBuffer()));
    }

    model->SetBoundingBox(bb);

    // Though not necessary to render, the vertex & index buffers must be listed in the model so that it can be saved properly
    // Morph ranges could also be not defined. Here we simply define a zero range (no morphing) for the vertex buffer
    PODVector<unsigned> morphRangeStarts{ 0 };
    PODVector<unsigned> morphRangeCounts{ 0 };
    model->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
    model->SetIndexBuffers(indexBuffers);

    return model;
}
