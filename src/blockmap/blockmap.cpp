/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed z the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../items/fedora.h"
#include "../items/helmet.h"
#include "../items/hardhat.h"
#include "../items/wizardhat.h"
#include "../items/scuba.h"
#include "../items/herring.h"
#include "../items/key.h"
#include "../items/life.h"
#include "../items/sardine.h"
#include "../creatures/bullhorn.h"
#include "../creatures/snail.h"
#include "../environs/mushroom.h"
#include "../environs/whipgrip.h"
#include "../environs/door.h"
#include "../environs/teleporter.h"
#include "../spawnmaster.h"
#include "../game.h"
#include "../tuxcam.h"

#include "block.h"
#include "tile.h"

#include "blockmap.h"

void BlockMap::RegisterObject(Context* context)
{
    context->RegisterFactory<BlockMap>();
    context->RegisterFactory<Block>();
    context->RegisterFactory<Tile>();

    context->RegisterFactory<Mushroom>();
    context->RegisterFactory<WhipGrip>();
    context->RegisterFactory<Door>();
    context->RegisterFactory<Teleporter>();

    context->RegisterFactory<Helmet>();
    context->RegisterFactory<HardHat>();
    WizardHat::RegisterObject(context);
    Fedora::RegisterObject(context);
    context->RegisterFactory<Scuba>();

    Herring::RegisterObject(context);
    context->RegisterFactory<Opener>();
    context->RegisterFactory<Life>();
    context->RegisterFactory<Sardine>();

    context->RegisterFactory<Bullhorn>();
    ZombieSnail::RegisterObject(context);
}

BlockMap::BlockMap(Context* context): Component(context),
    music_{ "" },
    zone_{ nullptr },
    blockGroups_{},
    mapSize_{},
    blockSize_{},
    rigidBody_{ nullptr },
    infiniteGrassNode_{ nullptr },
    liquidNode_{ nullptr },
    groundCollider_{ nullptr },
    collision_{ true },
    occupied_{}
{
}

void BlockMap::OnNodeSet(Node* node)
{
    if (!node)
        return;

    zone_ = GetScene()->CloneComponent(GetSubsystem<Renderer>()->GetDefaultZone())->Cast<Zone>();
    zone_->SetBoundingBox({ -Vector3::ONE * M_LARGE_VALUE, Vector3::ONE * M_LARGE_VALUE });

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetFriction(.42f);
    rigidBody_->SetRestitution(.1f);
    rigidBody_->SetCollisionLayer(LAYER(L_STATIC));

    groundCollider_ = node_->CreateComponent<CollisionShape>();

    infiniteGrassNode_ = GetScene()->CreateChild("Grass");
    infiniteGrassNode_->SetScale({ 192.f, 1.f, 192.f });
    infiniteGrassNode_->SetEnabled(false);

    StaticModel* grassPlane{ infiniteGrassNode_->CreateComponent<StaticModel>() };
    grassPlane->SetModel(RES(Model, "Models/Plane.mdl"));
    grassPlane->SetMaterial(RES(Material, "Materials/Grass.xml"));

    liquidNode_ = infiniteGrassNode_->CreateChild("Liquid");
    StaticModel* liquidPlane{ liquidNode_->CreateComponent<StaticModel>() };
    liquidPlane->SetModel(RES(Model, "Models/Plane.mdl"));
    liquidPlane->SetViewMask(0x80000000);

    RigidBody* liquidBody{ liquidNode_->CreateComponent<RigidBody>() };
    liquidBody->SetFriction(.42f);
    liquidBody->SetRestitution(.1f);
    liquidBody->SetCollisionLayer(LAYER(L_STATIC));
    liquidBody->SetTrigger(true);
}

void BlockMap::Clear()
{
    zone_->SetAmbientColor(GetSubsystem<Renderer>()->GetDefaultZone()->GetAmbientColor());
    music_.Clear();

    liquidNode_->SetEnabled(false);
    liquidNode_->RemoveComponent<CollisionShape>();
    liquidNode_->RemoveAllTags();

    node_->RemoveAllChildren();
    // Remove all colliders
    PODVector<CollisionShape*> colliders{};
    GetComponents<CollisionShape>(colliders);
    for (CollisionShape* c: colliders)
    {
        if (c != groundCollider_)
            c->Remove();
    }
    occupied_.Clear();

    GetSubsystem<SpawnMaster>()->Clear();
    HardHat::Clear();
    WizardHat::Clear();
}

void BlockMap::LoadXMLFile(XMLFile* file, bool collision)
{
    if (!file)
        return;

    collision_ = collision;
    const XMLElement mapElem{ file->GetRoot("blockmap") };
    LoadXML(mapElem);
}

bool BlockMap::LoadXML(const XMLElement& mapElem)
{
    if (!Animatable::LoadXML(mapElem))
        return false;

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    if (!spawn)
        return false;

    Clear();

    int itemSet{ -1 };
    int mushroomSet{ -1 };
    int characterSet{ -1 };
    mapSize_ = mapElem.GetIntVector3("map_size");
    blockSize_ = mapElem.GetVector3("block_size");
    HashMap<int, HashMap<int, Pair<String, StringVector> > > blockSets{};
    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };

    Vector3 floorPosition{ Vector3::DOWN * (blockSize_.y_ * (mapSize_.y_ - (mapSize_.y_ % 2)) * 0.5f + blockSize_.y_ * .5f) };
    infiniteGrassNode_->SetPosition(floorPosition);
    infiniteGrassNode_->SetEnabled(true);
    HardHat::SetGround(FloorToInt(floorPosition.y_));
    groundCollider_->SetStaticPlane(floorPosition);

    Color fogColor{ DEFAULT_FOG_COLOR };
    float waterLevel{};
    bool lava{};

    if (XMLElement attributesElem{ mapElem.GetChild("attributes") })
    {
        XMLElement attributeElem{ attributesElem.GetChild("attribute") };
        while (attributeElem)
        {
            if (attributeElem.GetAttribute("name") == "Fog Color")
                fogColor = Color{ attributeElem.GetVector4("value").Data() };
            else if (attributeElem.GetAttribute("name") == "Water Level")
                waterLevel = attributeElem.GetInt("value");
            else if (attributeElem.GetAttribute("name") == "Lava")
                lava = attributeElem.GetBool("value");
            else if (attributeElem.GetAttribute("name") == "Music")
                music_ = attributeElem.GetString("value");

            attributeElem = attributeElem.GetNext("attribute");
        }
    }

    zone_->SetFogColor(fogColor);

    if (waterLevel > 0.f)
    {
        liquidNode_->SetEnabled(true);
        liquidNode_->SetPosition(Vector3::UP * (waterLevel - .23f));
        liquidNode_->CreateComponent<CollisionShape>()->SetStaticPlane();
        StaticModel* liquidPlane{ liquidNode_->GetComponent<StaticModel>() };

        if (lava)
        {
            liquidNode_->AddTag("Lava");
            zone_->SetAmbientColor(GetSubsystem<Renderer>()->GetDefaultZone()->GetAmbientColor().Lerp(fogColor, .5f).Lerp(Color::BLACK, .2f));
            liquidPlane->SetMaterial(RES(Material, "Materials/Lava.xml"));
        }
        else
        {
            liquidPlane->SetMaterial(RES(Material, "Materials/Water.xml"));
            RenderPath* rp{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
            rp->SetShaderParameter("DistortionTint", liquidPlane->GetMaterial()->GetShaderParameter("WaterTint").GetVector3());
        }
    }

    TuxCam* camera{ GetSubsystem<Game>()->GetWorld().camera_ };
    if (camera)
        camera->SetReflectionsEnabled(HasWater());

    // Load blocksets
    while (blockSetRefElem)
    {
        unsigned blockSetId{ blockSetRefElem.GetUInt("id") };
        const String blockSetName{ blockSetRefElem.GetAttribute("name") };
        if (blockSetName.Contains("Items"))
            itemSet = blockSetId;
        else if (blockSetName.Contains("Mushroom"))
            mushroomSet = blockSetId;
        else if (blockSetName.Contains("Characters"))
            characterSet = blockSetId;

        XMLFile* blockSetFile{ RES(XMLFile, blockSetName) };
        if (!blockSetFile)
            return false;

        XMLElement blockSetElem{ blockSetFile->GetRoot("blockset") };
        XMLElement blockElem{ blockSetElem.GetChild("block") };

        while (blockElem)
        {
            blockSets[blockSetId][blockElem.GetUInt("id")].first_  = blockElem.GetAttribute("model");
            if (blockElem.HasAttribute("material"))
            {
                blockSets[blockSetId][blockElem.GetUInt("id")].second_ = StringVector{ blockElem.GetAttribute("material") };
            }
            else
            {
                StringVector materials{};
                XMLElement matElem{ blockElem.GetChild("material") };
                while (!matElem.IsNull())
                {
                    materials.Push(matElem.GetAttribute("name"));
                    matElem = matElem.GetNext("material");
                }

                blockSets[blockSetId][blockElem.GetUInt("id")].second_ = materials;
            }

            blockElem = blockElem.GetNext("block");
        }

        XMLElement tileElem{ blockSetElem.GetChild("tile") };
        while (tileElem)
        {
            blockSets[blockSetId][tileElem.GetUInt("id")].first_  = "";
            if (tileElem.HasAttribute("material"))
            {
                blockSets[blockSetId][tileElem.GetUInt("id")].second_ = StringVector{ tileElem.GetAttribute("material") };
            }

            tileElem = tileElem.GetNext("tile");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    XMLElement layerElem{ mapElem.GetChild("gridlayer") };

    while (layerElem)
    {
        const int layerId{ layerElem.GetInt("id") };
        XMLElement blockElem{ layerElem.GetChild("gridblock") };

        while (blockElem)
        {
            const int blockSetId{ blockElem.GetInt("set") };
            const int blockId{ blockElem.GetInt("block") };
            const String modelName{ blockSets[blockSetId][blockId].first_ };
            const IntVector3 blockCoords{ blockElem.GetIntVector3("coords") };
            const Vector3 blockPosition{ (blockCoords - (mapSize_ - VectorMod(mapSize_, { 2, 2, 2 })) * .5f) * blockSize_ };
            const Quaternion blockRotation{ blockElem.GetQuaternion("rot") };
            PODVector<Material*> materials{};
            for (const String& materialName: blockSets[blockSetId][blockId].second_)
                materials.Push(RES(Material, materialName));

            if (blockElem.HasAttribute("elevation"))
            {
                const String elevationString{ blockElem.GetAttribute("elevation") };
                const String normalsString{ blockElem.GetAttribute("normals") };
                const String spanString{ blockElem.GetAttribute("span") };
                HeightProfile profile{ HeightProfile::fromString(elevationString, normalsString, spanString) };

                if (!(profile.isFlat() && profile.min() + blockCoords.y_ == 0))
                {
                    Node* tileNode{ node_->CreateChild("Tile") };
                    tileNode->SetPosition(blockPosition);
                    tileNode->SetRotation(blockRotation);
                    Material* mat{ nullptr };
                    if (!materials.IsEmpty())
                        mat = materials.Front();

                    Tile* tile{ tileNode->CreateComponent<Tile>() };
                    tile->Initialize(profile, mat, collision_);
                }
            }
            else if (blockSetId == itemSet)
            {
                const String itemName{ modelName.Substring(modelName.FindLast('/') + 1, modelName.Length() - modelName.FindLast('/') - 5) };

                //Place item
                if (itemName == "Fedora")
                    spawn->Create<Fedora>(true, blockPosition);
                else if (itemName == "Helmet")
                    spawn->Create<Helmet>(true, blockPosition);
                else if (itemName == "HardHat")
                    spawn->Create<HardHat>(true, blockPosition);
                else if (itemName == "WizardHat")
                    spawn->Create<WizardHat>(true, blockPosition);
                else if (itemName == "Scuba")
                    spawn->Create<Scuba>(true, blockPosition);
                else if (itemName == "Herring")
                    spawn->Create<Herring>(true, blockPosition);
                else if (itemName == "Key")
                    spawn->Create<Opener>(true, blockPosition);
                else if (itemName == "Egg")
                    spawn->Create<Life>(true, blockPosition);
                else if (itemName == "Sardine")
                    spawn->Create<Sardine>(true, blockPosition);
            }
            else if (blockSetId == mushroomSet && spawn->GetScene())
            {
                Mushroom* mushroom{ spawn->Create<Mushroom>(true, blockPosition, blockRotation) };
                mushroom->GetNode()->SetParent(node_);
                mushroom->Initialize(RES(Model, modelName), {}, nullptr);
            }
            else if (modelName.Contains("BranchMid") && spawn->GetScene())
            {
                WhipGrip* whipGrip{ spawn->Create<WhipGrip>(true, blockPosition, blockRotation) };
                whipGrip->GetNode()->SetParent(node_);
                whipGrip->Initialize(RES(Model, modelName), materials, nullptr);
            }
            else if (modelName.Contains("Door") && spawn->GetScene())
            {
                Door* door{ spawn->Create<Door>(true, blockPosition, blockRotation) };
                door->GetNode()->SetParent(node_);
                door->Initialize(RES(Model, modelName), materials);
                door->Stretch();
            }
            else if (modelName.Contains("Teleporter") && spawn->GetScene())
            {
                Teleporter* teleporter{ spawn->Create<Teleporter>(true, blockPosition, blockRotation) };
                teleporter->GetNode()->SetParent(node_);
                teleporter->Initialize(RES(Model, modelName), materials);
                teleporter->Match(layerId);
            }
            else if (blockSetId == characterSet)
            {
                const String characterName{ modelName.Substring(modelName.FindLast('/') + 1, modelName.Length() - modelName.FindLast('/') - 5) };

                if (characterName == "Tux")
                {
                    SetGlobalVar("TuxSpawnPosition", blockPosition - blockSize_.y_ * .5f * Vector3::UP);
                    SetGlobalVar("TuxSpawnRotation", blockRotation);
                }
                else if (characterName == "Bullhorn")
                {
                    spawn->Create<Bullhorn>(true, blockPosition + Vector3::DOWN * .5f, blockRotation);
                }
                else if (characterName == "ZombieSnail")
                {
                    spawn->Create<ZombieSnail>(true, blockPosition + Vector3::DOWN * .5f, blockRotation);
                }
            }
            else
            {
                Node* blockNode{ node_->CreateChild("Block") };
                blockNode->SetPosition(blockPosition);
                blockNode->SetRotation(blockRotation);

                Block* block{ blockNode->CreateComponent<Block>() };
                block->Initialize(RES(Model, modelName), materials, nullptr, collision_);

                if (block->IsCollider())
                    occupied_.Insert(blockCoords);
            }

            blockElem = blockElem.GetNext("gridblock");
        }

        layerElem = layerElem.GetNext("gridlayer");
    }

    return true;
}

HashSet<IntVector3> BlockMap::GetUnoccupied(bool underwater) const
{
    HashSet<IntVector3> unoccupied{};

    for (int x{ -1 }; x <= mapSize_.x_; ++x)
    for (int y{ -1 }; y <= mapSize_.y_; ++y)
    for (int z{ -1 }; z <= mapSize_.z_; ++z)
    {
        if (underwater && HasWater() && y > liquidNode_->GetWorldPosition().y_)
            continue;

        const IntVector3 coords{ x, y, z };
        if (!occupied_.Contains(coords))
            unoccupied.Insert(coords);
    }

    return unoccupied;
}
