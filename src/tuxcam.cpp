/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <Dry/Audio/SoundListener.h>
#include "game.h"
#include "blockmap/blockmap.h"

#include "tuxcam.h"

TuxCam::TuxCam(Context* context) : LogicComponent(context),
    camera_{ nullptr },
    reflectionCamera_{ nullptr },
    renderTexture_{ nullptr },
    targetPos_{},
    targetVelocity_{},
    ceiling_{ 2.3f }
{
}

void TuxCam::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetPosition({ 5.f, 3.f, 2.f});
    node_->LookAt(Vector3::ZERO);
    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(55.f);
    camera_->SetNearClip(1.f);

    GetSubsystem<Audio>()->SetListener(node_->CreateComponent<SoundListener>());
}

void TuxCam::PostUpdate(float timeStep)
{
    World& world{ GetSubsystem<Game>()->GetWorld() };

    Node* infiniteGrassNode{ world.scene_->GetChild("Grass") };
    const float ignY{ infiniteGrassNode->GetWorldPosition().y_ };
    infiniteGrassNode->SetWorldPosition(node_->GetWorldPosition().ProjectOntoPlane(Vector3::UP) + ignY * Vector3::UP);

    Tux* tux{ world.tux_ };
    if (!tux)
        return;

    const bool sliding{ tux->IsSliding() && !tux->IsUnderwater() };
    const Node* tuxNode{ tux->GetGraphicsNode() };
    const RigidBody* tuxRb{ tuxNode->GetParent()->GetComponent<RigidBody>() };
    const Vector3 tuxPos{ tuxNode->GetWorldPosition() };

    targetVelocity_ = targetVelocity_.Lerp(tuxRb->GetLinearVelocity(), Min(Min(sinceReset_, 1.f), timeStep * 3.f));

    const Vector3 trace{ Vector3{ 1.f, (sliding ? .5f : 0.f), 1.f } * targetVelocity_ * .23f };
    targetPos_ = targetPos_.Lerp(tuxPos + trace + Vector3::UP, Min(1.0f, timeStep * 9.f));

    PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
    PhysicsRaycastResult result{};
    physicsWorld->RaycastSingle(result, { tuxPos + Vector3::UP, Vector3::UP }, 1.3f, LAYER(L_STATIC));
    ceiling_ = Lerp(ceiling_, Min(result.distance_, 2.3f), Min(1.f, timeStep * 5.f));

    const Vector3 camPos{ node_->GetWorldPosition() };
    const Vector3 delta{ camPos.ProjectOntoPlane(Vector3::UP, Vector3::UP * (targetPos_.y_ + ceiling_)) - targetPos_ };

    node_->SetPosition(targetPos_ + delta.Normalized() * 7.f - trace * .075f);
    node_->LookAt(targetPos_, node_->GetUp().Lerp(Vector3::UP, Min(1.0f, timeStep * 42.0f)));

    if (!GetSubsystem<Input>()->IsMouseVisible())
    {
        const Vector3 aim{ world.tux_->GetAim() + GetSubsystem<Input>()->GetMouseMove().x_ * .05f * Vector3::RIGHT };
        node_->RotateAround(targetPos_, { 0.f, aim.x_ * 2.3f, 0.f }, TS_WORLD);
    }

    sinceReset_ += timeStep;

    if (tux->IsFainted())
        GetSubsystem<Game>()->Fade(timeStep);

    RenderPath* rp{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
    rp->SetEnabled("Distortion", GetSubsystem<BlockMap>()->IsUnderwater(node_->GetWorldPosition()));
}

void TuxCam::Reset(const Vector3& position, const Vector3& direction)
{
    targetPos_ = position + Vector3::UP;
    targetVelocity_ = Vector3::ZERO;
    ceiling_ = 2.3f;
    sinceReset_ = 0.f;

    GetNode()->SetWorldPosition(targetPos_ - direction.Normalized() * 7.f);
    GetNode()->LookAt(targetPos_);

    GetSubsystem<Game>()->SetFadeMultiplier(1.f);
}

void TuxCam::SetReflectionsEnabled(bool enable)
{
    if (!camera_)
        return;

    if (!enable)
    {
        if (reflectionCamera_)
        {
            reflectionCamera_->Remove();
            reflectionCamera_ = nullptr;
        }

        return;
    }
    else if (!reflectionCamera_)
    {
        reflectionCamera_ = node_->CreateComponent<Camera>();
    }

    const float waterLevel{ GetSubsystem<BlockMap>()->GetWaterLevel() };
    const Plane waterPlane{ Vector3::UP, Vector3::UP * waterLevel };
    const Plane waterClipPlane{ waterPlane.normal_, (waterLevel - .023f) * Vector3::UP };

    reflectionCamera_->SetNearClip(camera_->GetNearClip());
    reflectionCamera_->SetFarClip(camera_->GetFarClip());
    reflectionCamera_->SetViewMask(0x7fffffff);
    reflectionCamera_->SetUseReflection(true);
    reflectionCamera_->SetReflectionPlane(waterPlane);
    reflectionCamera_->SetUseClipping(true);
    reflectionCamera_->SetClipPlane(waterClipPlane);
    reflectionCamera_->SetFov(camera_->GetFov());
    reflectionCamera_->SetAutoAspectRatio(false);

    RecreateReflectionTexture();

    SubscribeToEvent(E_BEGINRENDERING, DRY_HANDLER(TuxCam, HandleScreenMode));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(TuxCam, HandleScreenMode));
}

void TuxCam::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateReflectionAspectRatio();

    if (HasSubscribedToEvent(E_BEGINRENDERING))
        UnsubscribeFromEvent(E_BEGINRENDERING);
}

void TuxCam::UpdateReflectionAspectRatio()
{
    if (reflectionCamera_)
    {
        reflectionCamera_->SetAspectRatio(GetSubsystem<Graphics>()->GetRatio());
        RecreateReflectionTexture();
    }
}

void TuxCam::RecreateReflectionTexture()
{
    const IntVector2 windowSize{ GetSubsystem<Graphics>()->GetSize() };
    const unsigned textureSize{ Max(1u, ClosestPowerOfTwo(Max(windowSize.x_, windowSize.y_))) };

    if (renderTexture_)
    {
        if (renderTexture_->GetWidth() * 1u != textureSize)
        {
            renderTexture_->Release();
        }
        else
        {
            renderTexture_->GetRenderSurface()->GetViewport(0)->SetCamera(reflectionCamera_);
            return;
        }
    }

    renderTexture_ = new Texture2D{ context_ };
    renderTexture_->SetSize(textureSize, textureSize, Graphics::GetRGBFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->SetFilterMode(FILTER_BILINEAR);
    RenderSurface* surface{ renderTexture_->GetRenderSurface() };
    surface->SetUpdateMode(SURFACE_UPDATEVISIBLE);
    SharedPtr<Viewport> rttViewport{ new Viewport{ context_, GetScene(), reflectionCamera_ } };
    surface->SetViewport(0, rttViewport);

    Material* waterMat_ = RES(Material, "Materials/Water.xml");
    waterMat_->SetTexture(TU_DIFFUSE, renderTexture_);
}
