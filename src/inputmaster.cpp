/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "ui/gui.h"
#include "ui/settingsmenu.h"
#include "player.h"

#include "inputmaster.h"

InputMaster::InputMaster(Context* context): Object(context),
    keyBindingsMaster_{},
    buttonBindingsMaster_{},
    sinceMasterAction_{},
    keyBindingsPlayer_{},
    buttonBindingsPlayer_{},
    mouseBindingsPlayer_{},
    pressedJoystickButtons_{},
    axesPosition_{},
    pressedMouseButtons_{},
    controlledByPlayer_{}
{
    keyBindingsMaster_[KEY_UP]       = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_UP]    = MIA_UP;
    keyBindingsMaster_[KEY_DOWN]     = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_DOWN]  = MIA_DOWN;
    keyBindingsMaster_[KEY_LEFT]     = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_LEFT]  = MIA_LEFT;
    keyBindingsMaster_[KEY_RIGHT]    = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_RIGHT] = MIA_RIGHT;
    keyBindingsMaster_[KEY_KP_PLUS]  = keyBindingsMaster_[KEY_EQUALS]   = keyBindingsMaster_[KEY_RIGHTBRACKET] = buttonBindingsMaster_[CONTROLLER_BUTTON_RIGHTSHOULDER] = MIA_INCREMENT;
    keyBindingsMaster_[KEY_KP_MINUS] = keyBindingsMaster_[KEY_MINUS]    = keyBindingsMaster_[KEY_LEFTBRACKET]  = buttonBindingsMaster_[CONTROLLER_BUTTON_LEFTSHOULDER]  = MIA_DECREMENT;
    keyBindingsMaster_[KEY_RETURN]   = keyBindingsMaster_[KEY_KP_ENTER] = keyBindingsMaster_[KEY_SPACE]        = buttonBindingsMaster_[CONTROLLER_BUTTON_A] = MIA_CONFIRM;
    keyBindingsMaster_[KEY_ESCAPE]   = buttonBindingsMaster_[CONTROLLER_BUTTON_B]          = MIA_CANCEL;
    keyBindingsMaster_[KEY_P]        = buttonBindingsMaster_[CONTROLLER_BUTTON_START]      = MIA_PAUSE;

    keyBindingsPlayer_[1][KEY_W]     = PIA_MOVE_UP;
    keyBindingsPlayer_[1][KEY_S]     = PIA_MOVE_DOWN;
    keyBindingsPlayer_[1][KEY_A]     = PIA_MOVE_LEFT;
    keyBindingsPlayer_[1][KEY_D]     = PIA_MOVE_RIGHT;
    keyBindingsPlayer_[1][KEY_SHIFT] = PIA_RUN;
    keyBindingsPlayer_[1][KEY_SPACE] = PIA_JUMP;
    keyBindingsPlayer_[1][KEY_ALT]   = PIA_SLIDE;
    keyBindingsPlayer_[1][KEY_CTRL]  = PIA_USE;
    keyBindingsPlayer_[1][KEY_KP_PLUS]  = keyBindingsPlayer_[1][KEY_EQUALS] = keyBindingsPlayer_[1][KEY_RIGHTBRACKET] = PIA_NEXT;
    keyBindingsPlayer_[1][KEY_KP_MINUS] = keyBindingsPlayer_[1][KEY_MINUS]  = keyBindingsPlayer_[1][KEY_LEFTBRACKET]  = PIA_PREV;

    for (unsigned p{ 1 }; p <= 4; ++p)
    {
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_A]             = PIA_JUMP;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_X]             = PIA_RUN;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_B]             = PIA_SLIDE;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_Y]             = PIA_USE;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_LEFTSHOULDER]  = PIA_PREV;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_RIGHTSHOULDER] = PIA_NEXT;
    }

    mouseBindingsPlayer_[1][MOUSEB_LEFT]   = PIA_JUMP;
    mouseBindingsPlayer_[1][MOUSEB_RIGHT]  = PIA_SLIDE;
    mouseBindingsPlayer_[1][MOUSEB_MIDDLE] = PIA_USE;

//    keyBindingsPlayer_[2][KEY_UP]     = PIA_MOVE_UP;
//    keyBindingsPlayer_[2][KEY_DOWN]   = PIA_MOVE_DOWN;
//    keyBindingsPlayer_[2][KEY_LEFT]   = PIA_MOVE_LEFT;
//    keyBindingsPlayer_[2][KEY_RIGHT]  = PIA_MOVE_RIGHT;
//    keyBindingsPlayer_[2][KEY_LSHIFT] = PIA_SLIDE;

    for (int a{ 0 }; a < MIA_SCREENSHOT; ++a)
        sinceMasterAction_[a] = masterInterval_;

    SubscribeToEvent(E_GAMESTATUSCHANGED, DRY_HANDLER(InputMaster, HandleGameStatusChanged));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(InputMaster, HandleUpdate));
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(InputMaster, HandleJoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_MOUSEBUTTONDOWN, DRY_HANDLER(InputMaster, HandleMouseButtonDown));
    SubscribeToEvent(E_MOUSEBUTTONUP, DRY_HANDLER(InputMaster, HandleMouseButtonUp));
}

void InputMaster::HandleGameStatusChanged(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[GameStatusChanged::P_STATUS].GetUInt() != GS_PLAY)
    {
        keyBindingsMaster_[KEY_ESCAPE] = MIA_CANCEL;
        repeatedCancel_ = true;

        return;
    }

    keyBindingsMaster_[KEY_ESCAPE] = MIA_PAUSE;

    pressedJoystickButtons_.Clear();
    pressedKeys_.Clear();
    pressedMouseButtons_.Clear();
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    for (int a{ 0 }; a < MIA_SCREENSHOT; ++a)
    {
        const float factor{ 1.f + ((a == MIA_INCREMENT || a == MIA_DECREMENT) && GetSubsystem<GUI>()->GetSettingsMenu()->IsVisible()) };
        sinceMasterAction_[a] += eventData[Update::P_TIMESTEP].GetFloat() * factor;
    }

    Game* game{ GetSubsystem<Game>() };

    InputActions activeActions{};
    for (Player* p: game->GetPlayers())
    {
        const int pId{ p->GetPlayerId() };
        activeActions.player_[pId] = {};
    }

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (keyBindingsMaster_.Contains(key))
        {
            const MasterInputAction action{ keyBindingsMaster_[key] };

            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }
        //Check for player key presses
        if (game->GetStatus() == GS_PLAY)
        {
            for (Player* p: game->GetPlayers())
            {
                const int pId{ p->GetPlayerId() };

                if (keyBindingsPlayer_[pId].Contains(key))
                {
                    const PlayerInputAction action{ keyBindingsPlayer_[pId][key] };

                    if (!activeActions.player_[pId].Contains(action))
                        activeActions.player_[pId].Push(action);
                }
            }
        }
    }
    //Check for joystick button presses
    for (Player* p: game->GetPlayers())
    {
        const int pId{ p->GetPlayerId() };
        const int jId{ pId - 1 };

        for (int button: pressedJoystickButtons_[jId])
        {
            //Check for master button presses
            if (buttonBindingsMaster_.Contains(button))
            {
                const MasterInputAction action{ buttonBindingsMaster_[button] };

                if (!activeActions.master_.Contains(action))
                    activeActions.master_.Push(action);
            }
            //Check for player button presses
            if (game->GetStatus() == GS_PLAY)
            {
                if (buttonBindingsPlayer_[pId].Contains(button))
                {
                    const PlayerInputAction action{ buttonBindingsPlayer_[pId][button] };

                    if (!activeActions.player_[pId].Contains(action))
                        activeActions.player_[pId].Push(action);
                }
            }
        }

        //Menu stick
//        const float leftX{ axesPosition_[jId][CONTROLLER_AXIS_LEFTX] };
//        const float leftY{ axesPosition_[jId][CONTROLLER_AXIS_LEFTY] };
//        if (leftY < -.125f && !activeActions.master_.Contains(UP))    activeActions.master_.Push(UP);
//        if (leftY >  .125f && !activeActions.master_.Contains(DOWN))  activeActions.master_.Push(DOWN);
//        if (leftX < -.125f && !activeActions.master_.Contains(LEFT))  activeActions.master_.Push(LEFT);
//        if (leftX >  .125f && !activeActions.master_.Contains(RIGHT)) activeActions.master_.Push(RIGHT);
    }

    //Check for mouse button presses
    if (game->GetStatus() == GS_PLAY)
    {
        const int player{ 1 };
        for (int button: pressedMouseButtons_[player])
        {
            if (mouseBindingsPlayer_[player].Contains(button))
            {
                const PlayerInputAction action{ mouseBindingsPlayer_[player][button] };

                if (!activeActions.player_[player].Contains(action))
                    activeActions.player_[player].Push(action);
            }
        }
    }

    if (!activeActions.master_.Contains(MIA_CONFIRM)) repeatedConfirm_ = false;
    if (!activeActions.master_.Contains(MIA_CANCEL))  repeatedCancel_  = false;

    //Throttle menu repeat
    PODVector<MasterInputAction> masterActions{};
    for (MasterInputAction a: activeActions.master_)
    {
        if (a == MIA_CONFIRM && repeatedConfirm_ == true)
            continue;
        if (a == MIA_CANCEL  && repeatedCancel_  == true)
            continue;

        if (sinceMasterAction_[a] >= masterInterval_)
        {
            masterActions.Push(a);
            sinceMasterAction_[a] = 0.f;

            if (a == MIA_CONFIRM) repeatedConfirm_ = true;
            if (a == MIA_CANCEL)  repeatedCancel_  = true;
        }
    }
    activeActions.master_ = masterActions;

    //Handle the registered actions
    HandleActions(activeActions);
}

void InputMaster::HandleActions(const InputActions& actions)
{
    Game* game{ GetSubsystem<Game>() };

    //Handle master actions
    if (!actions.master_.IsEmpty())
    {
        if (game->GetStatus() == GS_PLAY)
        {
            if (actions.master_.Contains(MIA_PAUSE))
                game->TogglePause();
        }
        else
        {
            if (actions.master_.Contains(MIA_CANCEL))
            {
                GUI* gui{ GetSubsystem<GUI>() };
                gui->HandleCancel();
            }
            else if (actions.master_.Contains(MIA_CONFIRM))
            {
                UIElement* focusElem{ GetSubsystem<UI>()->GetFocusElement() };

                if (focusElem)
                {
                    VariantMap eventData{};
                    eventData.Insert({ ClickEnd::P_ELEMENT, Variant{ focusElem } });
                    eventData.Insert({ ClickEnd::P_BUTTON, Variant{ MOUSEB_LEFT } });
                    focusElem->SendEvent(E_CLICK, eventData);
                    focusElem->SendEvent(E_CLICKEND, eventData);
                }
            }

            VariantMap eventData{};
            VariantVector masterActions{};

            for (MasterInputAction a: actions.master_)
                masterActions.Push(Variant{ static_cast<unsigned>(a) });

            eventData.Insert({ MenuInput::P_ACTIONS, Variant{ masterActions } });
            SendEvent(E_MENUINPUT, eventData);
        }
    }

    if (game->GetStatus() != GS_PLAY)
        return;

    //Handle player actions
    for (Player* p: game->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        PODVector<PlayerInputAction> playerInputActions{ *(actions.player_[pId]) };

        Controllable* controlled{ controlledByPlayer_[pId] };
        if (controlled)
        {
            Vector3 stickMove{ Vector3(axesPosition_[pId - 1][0], 0.0f, -axesPosition_[pId - 1][1]) };
            Vector3 stickAim{  Vector3(axesPosition_[pId - 1][2], 0.0f, -axesPosition_[pId - 1][3]) };

            controlled->SetMove(GetMoveFromActions(playerInputActions) + stickMove);
            controlled->SetAim(GetAimFromActions(playerInputActions) + stickAim);

            std::bitset<4> restActions{};
            const bool invertedRun{ true };
            restActions[PIA_RUN]   = playerInputActions.Contains(PIA_RUN) ^ invertedRun;
            restActions[PIA_JUMP]  = playerInputActions.Contains(PIA_JUMP);
            restActions[PIA_SLIDE] = playerInputActions.Contains(PIA_SLIDE);
            restActions[PIA_USE]   = playerInputActions.Contains(PIA_USE);

            controlled->SetActions(restActions);

            const int step{ playerInputActions.Contains(PIA_NEXT) - playerInputActions.Contains(PIA_PREV)};
            p->Step(step);
        }
    }
}

void InputMaster::Screenshot()
{
    Image screenshot(context_);
    Graphics* graphics{ GetSubsystem<Graphics>() };
    graphics->TakeScreenShot(screenshot);
    //Here we save in the Data folder with date and time appended
    String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
                Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_')+".png" };
    //Log::Write(1, fileName);
    screenshot.SavePNG(fileName);
}

void InputMaster::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const int key{ eventData[KeyDown::P_KEY].GetInt() };

    if (!pressedKeys_.Contains(key))
    {
        pressedKeys_.Push(key);

        if (keyBindingsMaster_.Contains(key))
            sinceMasterAction_[keyBindingsMaster_[key]] = masterInterval_;
    }

    switch (key)
    {
    case KEY_9: Screenshot(); break;
    default: break;
    }
}

void InputMaster::HandleKeyUp(StringHash /*eventType*/, VariantMap& eventData)
{
    const int key{ eventData[KeyUp::P_KEY].GetInt() };

    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoystickButtonDown(StringHash /*eventType*/, VariantMap &eventData)
{
    const int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    const ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };

    if (!pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Push(button);
}

void InputMaster::HandleJoystickButtonUp(StringHash /*eventType*/, VariantMap &eventData)
{
    const int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    const ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };

    if (pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(StringHash /*eventType*/, VariantMap& eventData)
{
    const int joystickId{ eventData[JoystickAxisMove::P_JOYSTICKID].GetInt() };
    const int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    const float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    axesPosition_[joystickId][axis] = PowN(position, 3);
}

void InputMaster::HandleMouseButtonDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const int player{ 1 };
    const MouseButton button{ static_cast<MouseButton>(eventData[MouseButtonDown::P_BUTTON].GetInt()) };

    if (!pressedMouseButtons_[player].Contains(button))
        pressedMouseButtons_[player].Push(button);
}

void InputMaster::HandleMouseButtonUp(StringHash /*eventType*/, VariantMap& eventData)
{
    const int player{ 1 };
    MouseButton button{ static_cast<MouseButton>(eventData[MouseButtonUp::P_BUTTON].GetInt()) };

    if (pressedMouseButtons_[player].Contains(button))
        pressedMouseButtons_[player].Remove(button);
}

Vector3 InputMaster::GetMoveFromActions(const PODVector<PlayerInputAction>& actions)
{
    return Vector3{ Vector3::RIGHT *
                    (actions.Contains(PIA_MOVE_RIGHT) - actions.Contains(PIA_MOVE_LEFT))
                    + Vector3::FORWARD *
                    (actions.Contains(PIA_MOVE_UP)    - actions.Contains(PIA_MOVE_DOWN)) };
}

Vector3 InputMaster::GetAimFromActions(const PODVector<PlayerInputAction>& actions)
{
    return Vector3{ Vector3::RIGHT *
                    (actions.Contains(PIA_AIM_E) - actions.Contains(PIA_AIM_W))
                    + Vector3::FORWARD *
                    (actions.Contains(PIA_AIM_N) - actions.Contains(PIA_AIM_S))

                    + Quaternion(45.0f, Vector3::UP) *
                    (Vector3::RIGHT *
                    (actions.Contains(PIA_AIM_SE) - actions.Contains(PIA_AIM_NW))
                    + Vector3::FORWARD *
                    (actions.Contains(PIA_AIM_NE) - actions.Contains(PIA_AIM_SW))) };
}

void InputMaster::SetPlayerControl(Player* player, Controllable* controllable)
{
    int playerId{ player->GetPlayerId() };

    if (controlledByPlayer_.Contains(playerId))
    {
        if (controlledByPlayer_[playerId] == controllable)
            return;

        controlledByPlayer_[playerId]->ClearControl();
    }

    controlledByPlayer_[playerId] = controllable;
}

Player* InputMaster::GetPlayerByControllable(Controllable* controllable)
{
    Game* game{ GetSubsystem<Game>() };

    for (int k: controlledByPlayer_.Keys())
    {
        if (controlledByPlayer_[k] == controllable)
            return game->GetPlayer(k);
    }

    return nullptr;
}

Controllable* InputMaster::GetControllableByPlayer(int playerId)
{
    return controlledByPlayer_[playerId];
}
