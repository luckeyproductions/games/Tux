/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"
#include "inputmaster.h"
#include "spawnmaster.h"
//#include "pathfinder.h"

#include "jukebox.h"
#include "scores.h"
#include "blockmap/blockmap.h"
#include "player.h"
#include "tuxcam.h"

#include "ui/sardinerunmenu.h"
#include "ui/dash.h"

#include "effect/beam.h"
#include "effect/poof.h"
#include "effect/stars.h"

#include "game.h"

void Game::RegisterObject(Context* context)
{
    BlockMap::RegisterObject(context);
    Tux::RegisterObject(context);

    context->RegisterFactory<TuxCam>();
    context->RegisterFactory<Poof>();
    context->RegisterFactory<Beam>();
    context->RegisterFactory<Stars>();
}

Game::Game(Context* context): Object(context),
    world_{},
    status_{ GS_MAIN },
    players_{ SharedPtr<Player>(new Player{ 1, context_ }) }
{
    context_->RegisterSubsystem<Scores>();
    context_->RegisterSubsystem<JukeBox>();
//    context_->RegisterSubsystem<PathFinder>();

    AudioSettings* audioSettings{ GetSubsystem<Settings>()->GetAudioSettings() };
    const float musicGain{ audioSettings->GetAttribute(AS_MUSIC_ENABLED).GetBool() * audioSettings->GetAttribute(AS_MUSIC_GAIN).GetFloat() };
    const float effectGain{ audioSettings->GetAttribute(AS_SAMPLES_ENABLED).GetBool() * audioSettings->GetAttribute(AS_SAMPLES_GAIN).GetFloat() };

    Audio* audio{ GetSubsystem<Audio>() };
    audio->SetMasterGain(SOUND_MUSIC, musicGain);
    audio->SetMasterGain(SOUND_EFFECT, effectGain);
}

void Game::CreateMenuScene()
{
    menuScene_ = CreateScene(true);
}

Scene* Game::CreateScene(bool mainMenu)
{
    Scene* scene{ new Scene{ context_ } };
    scene->CreateComponent<Octree>();

    if (!mainMenu)
    {
        scene->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);

        PhysicsWorld* physicsWorld{ scene->CreateComponent<PhysicsWorld>() };
        physicsWorld->SetGravity(Vector3::DOWN * 23.0f);
        physicsWorld->SetInternalEdge(true);
        //    physicsWorld->SetSplitImpulse(true);
    }

    //Light
    Node* sunNode{ scene->CreateChild("Light") };
    sunNode->SetPosition(Vector3{ 1.7f, 2.3f, -0.5f });
    sunNode->LookAt(Vector3::ZERO);
    Light* sun{ sunNode->CreateComponent<Light>() };
    sun->SetLightType(LIGHT_DIRECTIONAL);
    sun->SetBrightness(.6f);
    sun->SetCastShadows(true);
    sun->SetShadowIntensity(.17f);
    sun->SetShadowBias({ .000017f, 2.3f });
    sun->SetShadowMaxExtrusion(100.f);
//    sun->SetShadowCascade({ 17.f, 23.f, 34.f, 55.f, .75f });
    sun->SetShadowCascade({ 55.f, 0.f, 0.f, 0.f, 0.95f });

    Node* ambientLightNode{ scene->CreateChild("AmbientLight") };
    ambientLightNode->SetPosition(Vector3::UP);
    ambientLightNode->LookAt(Vector3::ZERO);
    Light* ambientLight{ ambientLightNode->CreateComponent<Light>() };
    ambientLight->SetLightType(LIGHT_DIRECTIONAL);
    ambientLight->SetBrightness(.23f);
    ambientLight->SetSpecularIntensity(0.f);

    Node* backLightNode{ scene->CreateChild("AmbientLight") };
    backLightNode->SetPosition({ -2.3f, 0.f, .23f });
    backLightNode->LookAt(Vector3::ZERO);
    Light* backLight{ backLightNode->CreateComponent<Light>() };
    backLight->SetLightType(LIGHT_DIRECTIONAL);
    backLight->SetBrightness(.1f);
    backLight->SetSpecularIntensity(0.f);

    if (!mainMenu)
    {
        world_.scene_ = scene;
        //Tux!
        world_.tux_ = GetSubsystem<SpawnMaster>()->Create<Tux>();

        GetSubsystem<InputMaster>()->SetPlayerControl(GetPlayer(1), world_.tux_);

        //Camera
        world_.camera_ = GetSubsystem<SpawnMaster>()->Create<TuxCam>();
    }

    //Level
    if (!mainMenu)
    {
        world_.level_ = scene->CreateChild("Level")->CreateComponent<BlockMap>();
        context_->RegisterSubsystem(world_.level_);
    }
    else
    {
        const float menuMapWidth{ 42.f };

        for (bool shifted: { false, true })
        {
            Node* blockMapNode{ scene->CreateChild("BlockMap") };
            BlockMap* blockMap{ blockMapNode->CreateComponent<BlockMap>() };
            blockMap->LoadXMLFile(RES(XMLFile, "Maps/Menu.emp"), false);

            if (shifted)
                blockMapNode->Translate(Vector3::RIGHT * menuMapWidth);
        }

        const Vector3 camStartPos{ 0.f, 3.f, -34.f };
        Node* cameraNode{ scene->CreateChild("Camera") };
        cameraNode->SetPosition(camStartPos);
        cameraNode->LookAt(Vector3::ZERO);
        ValueAnimation* camSlide{ new ValueAnimation(context_) };
        camSlide->SetKeyFrame(0.f, camStartPos);
        camSlide->SetKeyFrame(55.f, camStartPos + Vector3::RIGHT * menuMapWidth);
        cameraNode->SetAttributeAnimation("Position", camSlide);

        Camera* camera{ cameraNode->CreateComponent<Camera>() };
        camera->SetFarClip(55.f);
        camera->SetNearClip(1.f);
        camera->SetFov(30.f);
        Viewport* viewport{ new Viewport{ context_, scene, camera } };
        GetSubsystem<Renderer>()->SetViewport(0, viewport);
    }

    return scene;
}

void Game::SetStatus(GameStatus status)
{
    if (status_ != status)
    {
        VariantMap eventData{};
        eventData[GameStatusChanged::P_OLDSTATUS] = status_;
        eventData[GameStatusChanged::P_STATUS] = status;

        status_ = status;

        SendEvent(E_GAMESTATUSCHANGED, eventData);
    }

    if (status_ == GS_MAIN)
    {
        Viewport* viewPort{ GetSubsystem<Renderer>()->GetViewport(0) };
        viewPort->SetCamera(menuScene_->GetChild("Camera")->GetComponent<Camera>());
        viewPort->SetScene(menuScene_);
        SetFadeMultiplier(1.f);
        menuScene_->SetUpdateEnabled(true);

        GetSubsystem<JukeBox>()->Play(JukeBox::MUSIC_MENU);

        RenderPath* rp{ GetSubsystem<Renderer>()->GetDefaultRenderPath() };
        rp->SetEnabled("Distortion", false);
    }
    else if (status == GS_PLAY)
    {
        Viewport* viewPort{ GetSubsystem<Renderer>()->GetViewport(0) };
        viewPort->SetCamera(world_.scene_->GetChild("TuxCam")->GetComponent<Camera>());
        viewPort->SetScene(world_.scene_);
    }
}

void Game::StartNew(GameMode mode, const String& map)
{
    menuScene_->SetUpdateEnabled(false);

    if (!world_.scene_)
        world_.scene_ = CreateScene();
    else
        world_.scene_->SetElapsedTime(0.f);

    UnpauseScene();
    SetStatus(GS_PLAY);

    GetSubsystem<Inventory>()->Reset();

    mode_ = mode;
    LoadMap(map);

    const String& music{ world_.level_->GetMusic() };
    if (music.IsEmpty())
    {
        if (mode_ == GM_STORY)
            GetSubsystem<JukeBox>()->Play(JukeBox::MUSIC_STORY);
        else if (mode_ == GM_RUN)
            GetSubsystem<JukeBox>()->Play(JukeBox::MUSIC_RUN);
    }
    else
    {
        GetSubsystem<JukeBox>()->Play(music);
    }

    Dash* dash{ GetSubsystem<Dash>() };
    dash->SetVisible(true);
    dash->UpdateSardineCount();
    dash->SetTimerScene(world_.scene_);
}

void Game::LoadMap(const String& mapName)
{
    String fullMapName{ mapName };

    if (!fullMapName.EndsWith(".emp", false))
        fullMapName = fullMapName.Append(".emp");

    world_.level_->LoadXMLFile(RES(XMLFile, fullMapName));
    context_->SetGlobalVar("Level", mapName);

    const Vector3 spawnPos{ GetGlobalVar("TuxSpawnPosition").GetVector3() };
    const Quaternion spawnRot{ GetGlobalVar("TuxSpawnRotation").GetQuaternion() };
    world_.tux_->Reset(spawnPos, spawnRot, mode_ != GM_STORY);
    const Vector3 camDir{ (spawnRot * Vector3::FORWARD * 6.f) };
    world_.camera_->Reset(spawnPos, camDir.ProjectOntoPlane(Vector3::UP, (spawnPos.y_ + 2.3f) * Vector3::UP));
}

void Game::TogglePause()
{
    GUI* gui{ GetSubsystem<GUI>() };
    if (status_ == GS_PLAY)
    {
        world_.scene_->SetTimeScale(0.f);
        world_.scene_->SetUpdateEnabled(false);
        SetStatus(GS_PAUSED);

        gui->ShowSardineRun();
    }
    else if (status_ == GS_PAUSED)
    {
        UnpauseScene();
        SetStatus(GS_PLAY);
    }
}

void Game::UnpauseScene()
{
    world_.scene_->SetTimeScale(1.f);
    world_.scene_->SetUpdateEnabled(true);
}

Vector<SharedPtr<Player> > Game::GetPlayers() const
{
    return players_;
}


Player* Game::GetPlayer(int playerID) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerID)
            return p;
    }

    return nullptr;
}

Player* Game::GetNearestPlayer(const Vector3& pos)
{
    InputMaster* im{ GetSubsystem<InputMaster>() };
    Player* nearest{};

    for (Player* p : players_)
    {
        if (p->IsAlive())
        {
            if (!nearest
                    || (im->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition().DistanceToPoint(pos) <
                        im->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition().DistanceToPoint(pos)))
            {
                nearest = p;
            }
        }
    }
    return nearest;
}

void Game::SetFadeMultiplier(float fadeMultiplier)
{
    RenderPath* renderPath{ GetSubsystem<Renderer>()->GetViewport(0)->GetRenderPath() };
    renderPath->SetShaderParameter("FadeMultiplier", fadeMultiplier);
    renderPath->SetEnabled("Fade", !Equals(fadeMultiplier, 1.f));
}

void Game::Fade(float timeStep)
{
    const float currentFade{ GetSubsystem<Renderer>()->GetViewport(0)->GetRenderPath()->GetShaderParameter("FadeMultiplier").GetFloat() };
    if (currentFade <= 0.f)
        return;;

    const float fadeSpeed{ PowN(1.25f - currentFade, 2) };
    SetFadeMultiplier(Max(0.f, currentFade - timeStep * fadeSpeed));
}
