/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../tux.h"

#include "mushroom.h"

Mushroom::Mushroom(Context* context): Environ(context),
    model_{ nullptr },
    t_{ 5.f },
    extra_{ false }
{
}

void Mushroom::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Environ::OnNodeSet(node);
    model_ = node_->CreateComponent<AnimatedModel>();
    model_->SetCastShadows(true);

    SetUpdateEventMask(USE_UPDATE);
}

void Mushroom::Initialize(Model* model, const PODVector<Material*>& materials, Model* collisionMesh)
{
    Environ::Initialize(model, materials, collisionMesh);

    model_->SetModel(model);
    model_->SetMaterial(RES(Material, "Materials/VCol.xml"));

    const bool offcenter{ model->GetName().At(model->GetName().Length() - 5) == 'B' };

    Node* triggerNode{ node_->CreateChild("Trigger") };
    const Vector3 bbSize{ VectorRound(model->GetBoundingBox().Size()) };
    triggerNode->SetPosition((bbSize.y_ - .25f) * Vector3::UP + offcenter * Vector3{ .5f, 0.f, .5f });
    RigidBody* trigger{ triggerNode->CreateComponent<RigidBody>() };
    trigger->SetTrigger(true);
    trigger->SetCollisionLayer(LAYER(L_STATIC));
    trigger->SetCollisionMask(LAYER(L_PLAYER));
    CollisionShape* triggerShape{ triggerNode->CreateComponent<CollisionShape>() };
    triggerShape->SetCylinder(bbSize.x_ - 2/3.f, .1f);

    SubscribeToEvent(triggerNode, E_NODECOLLISION, DRY_HANDLER(Mushroom, HandleNodeCollision));
}

void Mushroom::Update(float timeStep)
{
    const float l{ 1.25f + .25f * extra_ };

    if (t_ < l)
        t_ += timeStep;
    else
        return;

    // Elastic interpolation
    const float f{ 1.f / l };
    const float t{ Min(t_, l)  };
    const float amp{ l * PowN(1.f -  t * f, 4) };
    const float w{ amp * Sin(t * 1350.f) };

    model_->SetMorphWeight(0, w);
}

void Mushroom::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };

    if (otherBody->GetLinearVelocity().y_ <= 0.f && t_ > .1f)
    {
        t_ = 0.f;

        const float impulse{ Sqrt(4.f - Round(model_->GetBoundingBox().Size().y_)) };
        bool bounce{ false };
        if (KinematicCharacterController* character{ otherNode->GetComponent<KinematicCharacterController>() })
        {
            Controllable* controllable{ otherNode->GetDerivedComponent<Controllable>() };
            bounce = controllable->Bounce();
            character->Jump(Vector3::UP * (impulse * 7.f + bounce * 3.f));

            if (Tux* tux = otherNode->GetComponent<Tux>())
                tux->FakeJump();
        }
        else
        {
            otherBody->ApplyImpulse(Vector3::UP * impulse * 23.f);
        }

        PlaySample("Bounce.wav", bounce * .125f + impulse - .125f, .125f * impulse + .3f);
        extra_ = bounce;
    }
}
