/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../effect/beam.h"

#include "teleporter.h"

Teleporter::Teleporter(Context* context): Environ(context),
    other_{ nullptr },
    layer_{ 0 }
{
}


void Teleporter::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Environ::OnNodeSet(node);

    model_ = node_->CreateComponent<StaticModel>();
    model_->SetCastShadows(true);

//    SetUpdateEventMask(USE_UPDATE);
}

void Teleporter::Initialize(Model* model, const PODVector<Material*>& materials, Model* collisionMesh)
{
    Environ::Initialize(model, materials, collisionMesh);
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED);

    model_->SetModel(model);
    if (materials.Size() == 1)
        model_->SetMaterial(materials.Front());
    else for (unsigned m{ 0u }; m < materials.Size(); ++m)
        model_->SetMaterial(m, materials.At(m));
}

void Teleporter::Match(int layer)
{
    layer_ = layer;

    PODVector<Teleporter*> teleporters{};
    GetNode()->GetParent()->GetComponents<Teleporter>(teleporters, true);

    for (Teleporter* t: teleporters)
    {
        if (t->GetLayer() == layer_ && t != this)
        {
            other_ = t;
            other_->SetOther(this);
            return;
        }
    }
}

void Teleporter::Sparkle() const
{
    Node* beamNode{ node_->CreateChild("Beam") };
    beamNode->CreateComponent<Beam>();
}

