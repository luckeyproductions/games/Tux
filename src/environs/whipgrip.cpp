/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "whipgrip.h"

WhipGrip::WhipGrip(Context* context): Environ(context),
    model_{ nullptr },
    constraint_{ nullptr },
    swingAxis_{ Vector3::FORWARD }
{
}

void WhipGrip::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Environ::OnNodeSet(node);

    node_->SetName("Whipgrip");
    model_ = node_->CreateComponent<StaticModel>();
    model_->SetCastShadows(true);

    // Debug
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(WhipGrip, HandlePostRenderUpdate));
}

void WhipGrip::Initialize(Model* model, const PODVector<Material*>& materials, Model* collisionMesh)
{
    Environ::Initialize(model, materials, collisionMesh);

    model_->SetModel(model);
    if (materials.Size() == 1)
        model_->SetMaterial(materials.Front());
    else for (unsigned m{ 0u }; m < materials.Size(); ++m)
        model_->SetMaterial(m, materials.At(m));

    if (MapHasCollision())
    {
        Node* triggerNode{ node_->CreateChild("Trigger") };
        RigidBody* trigger{ triggerNode->CreateComponent<RigidBody>() };
        trigger->SetKinematic(true);
        trigger->SetTrigger(true);
        trigger->SetCollisionLayerAndMask(LAYER(L_WHIPGRIP), M_MAX_UNSIGNED);
        CollisionShape* triggerShape{ triggerNode->CreateComponent<CollisionShape>() };
        triggerShape->SetSphere(1.f);

        constraint_ = node_->CreateComponent<Constraint>();
        constraint_->SetConstraintType(CONSTRAINT_POINT);
    }
}

void WhipGrip::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    debug->AddSphere({ node_->GetChild("Trigger")->GetWorldPosition(), .5f }, Color::ORANGE, false);
}
