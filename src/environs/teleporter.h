/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TELEPORTER_H
#define TELEPORTER_H

#include "environ.h"


class Teleporter: public Environ
{
    DRY_OBJECT(Teleporter, Environ);

public:
    Teleporter(Context* context);

    void Initialize(Model* model = nullptr, const PODVector<Material*>& materials = {}, Model* collisionMesh = nullptr) override;
    void Match(int layer);
    void SetOther(Teleporter* teleporter) { other_ = teleporter; }
    int GetLayer() const { return layer_; }
    Teleporter* GetOther() const { return other_; }

    void Sparkle() const;
protected:
    void OnNodeSet(Node* node) override;

private:
    StaticModel* model_;
    Teleporter* other_;
    int layer_;
};

#endif // TELEPORTER_H
