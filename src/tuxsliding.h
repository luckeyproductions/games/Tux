/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TUXSLIDING_H
#define TUXSLIDING_H

#include "mastercontrol.h"

#define OFFSET_SLIDER Vector3{ 0.f, 2/5.f, 0.f }
#define OFFSET_GRAPHICS Vector3{ 0.f, .05f, 0.f }

#define INTERVAL_DASH 1.1f

class TuxSliding: public LogicComponent
{
    DRY_OBJECT(TuxSliding, LogicComponent);

public:
    friend class Tux;
    TuxSliding(Context* context);

    void Launch();
    void Dash();

    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandleNodeCollisionStart(const StringHash eventType, VariantMap& eventData);
    void HandlePostRenderUpdate(const StringHash eventType, VariantMap& eventData);
    bool IsUnderwater() const;

    Vector3 SurfaceNormal();

    Tux* tux_;
    Vector3 move_;
    RigidBody* rigidBody_;
    CollisionShape* collisionShape_;
    Vector3 smoothVelocity_;
    float sinceDash_;
};

DRY_EVENT(E_HARDBUMP, HardBump)
{
    DRY_PARAM(P_STUN, Stun);            // bool
    DRY_PARAM(P_BURN, Burn);            // bool
}

#endif // TUXSLIDING_H
