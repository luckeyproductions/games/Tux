
#ifndef STARS_H
#define STARS_H

#include "../luckey.h"


class Stars: public Component
{
    DRY_OBJECT(Stars, Component);

public:
    Stars(Context* context);
    ~Stars() = default;
    void SetSpark(bool spark);

protected:
    void OnNodeSet(Node* node) override;

private:
    void HandleSceneUpdate(StringHash eventType, VariantMap& eventData);

    bool spark_;
    BillboardSet* billboards_;
    float t_;
};

#endif // STARS_H
