/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "stars.h"
#include "poof.h"
#define V1 Vector3::ONE

Poof::Poof(Context* context): ParticleSystem(context)
{
    const Vector3 minDir{ (-V1 + Vector3::UP) * 1/3.f };
    const Vector3 maxDir{ ( V1 + Vector3::UP) * 1/3.f };
    const TypedPolynomial<Vector3> minPosPoly{ { minDir * 3/4.f, minDir.ProjectOntoPlane(Vector3::UP) * 23.f, minDir.ProjectOntoPlane(Vector3::UP) * 5.f, -minDir.ProjectOntoPlane(Vector3::UP).Normalized() * 34.f, Vector3::UP * 17.f } };
    const TypedPolynomial<Vector3> maxPosPoly{ { maxDir * 3/4.f, maxDir.ProjectOntoPlane(Vector3::UP) * 23.f, maxDir.ProjectOntoPlane(Vector3::UP) * 5.f, -maxDir.ProjectOntoPlane(Vector3::UP).Normalized() * 34.f, Vector3::UP * 17.f } };
    const TypedBipolynomial<Vector3> posBipoly{ minPosPoly, maxPosPoly };
    particleGenerator_.position_ = posBipoly;

    const TypedPolynomial<Vector3> minScalePoly{ { V1 * .2f, V1 * .23f, V1 * -1.f, V1 * -.5f } };
    const TypedPolynomial<Vector3> maxScalePoly{ { V1 * .3f, V1 * .17f, V1 *   .5f, V1 * -.5f } };
    const TypedBipolynomial<Vector3> scaleBipoly{ minScalePoly, maxScalePoly };
    particleGenerator_.scale_ = scaleBipoly;

    particleGenerator_.color_ = TypedBipolynomial<Color>{ TypedPolynomial<Color>{ { Color{ .64f, .62f, .6f, .1f },  Color{ -.05f, -.05f, -.05f, .5f }, Color{ -.1f, -.1f, -.1f, -2.f } } },
                                                          TypedPolynomial<Color>{ { Color{ .7f,  .72f,  .55f, .05f }, Color{ -.05f, -.05f, -.05f, .3f }, Color{  .1f,  .1f,  .1f, -3.f } } }};
    lifeExpectancy_ = 1/2.f;
    emitting_ = 64;
    Trigger();
    emitting_ = 0;
}

void Poof::OnNodeSet(Node* node)
{
    ParticleSystem::OnNodeSet(node);

    if (!node)
        return;

    billboards_ = node_->CreateComponent<BillboardSet>();
    billboards_->SetMaterial(RES(Material, "Materials/Poof.xml"));

    node_->CreateChild("Stars")->CreateComponent<Stars>()->SetSpark(true);
}

void Poof::HandleUpdate(const StringHash eventType, VariantMap& eventData)
{
    ParticleSystem::HandleUpdate(eventType, eventData);

    if (particles_.IsEmpty() && GetNode()->GetNumChildren() == 0u)
        node_->Remove();
}
