
#include "stars.h"

Stars::Stars(Context* context): Component(context),
    spark_{ false },
    billboards_{ nullptr },
    t_{}
{
}

void Stars::OnNodeSet(Node* node)
{
    if (!node)
        return;

    billboards_ = node_->CreateComponent<BillboardSet>();
    billboards_->SetMaterial(RES(Material, "Materials/Star.xml"));
    billboards_->SetSorted(true);
    billboards_->SetNumBillboards(5);
    billboards_->SetViewMask(0x80000000);
    for (unsigned b{ 0 }; b < billboards_->GetNumBillboards(); ++b)
    {
        Billboard* billboard{ billboards_->GetBillboard(b) };
        billboard->size_ = Vector2::ONE * .125f;
        billboard->enabled_ = true;
    }

    SubscribeToEvent(GetScene(), E_SCENEUPDATE, DRY_HANDLER(Stars, HandleSceneUpdate));
}

void Stars::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!spark_)
        node_->SetWorldRotation(Quaternion::IDENTITY);

    const float dt{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };

    for (unsigned b{ 0 }; b < billboards_->GetNumBillboards(); ++b)
    {
        Billboard* billboard{ billboards_->GetBillboard(b) };
        billboard->position_ = Quaternion{ b * 72.f + !spark_ * t_ * 235.f, Vector3::UP } * ((Cbrt(t_) * spark_ * 1.7f + 2/3.f) * Vector3{ 0.f, .25f, .75f });
        billboard->rotation_ = 17.f * Sin(23.f * b + t_ * (180.f + 360.F * spark_));
        billboard->color_ = 2/3.f - t_;
    }

    if (spark_ && t_ > 1/3.f)
    {
        node_->Remove();
        return;
    }

    billboards_->Commit();
    t_ += dt;
}

void Stars::SetSpark(bool spark)
{
    spark_ = spark;

    for (Billboard& bb: billboards_->GetBillboards())
        bb.size_ = Vector2::ONE * (.125f + .05f * spark_);
    billboards_->Commit();
}
