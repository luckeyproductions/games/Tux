/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef TUX_H
#define TUX_H

#include "tuxsliding.h"
#include "controllable.h"

#define GRAVITY Vector3::DOWN * 23.f
#define STUN_LENGTH 2.3f

class Item;

class Tux: public Controllable
{
    DRY_OBJECT(Tux, Controllable);

public:
    static void RegisterObject(Context* context);

    Tux(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    void Reset(const Vector3 position, const Quaternion rotation, bool fillHealth);
    void SetHat(const String& hatName);
    String CurrentHatName() const;

    bool HasToughHat() const;
    bool HasScuba() const { return CurrentHatName().Contains("Scuba"); }

    TuxSliding* StartSliding(bool launch = false);
    void EndSliding();
    bool IsSliding() const { return sliding_; }
    float IsClinging() const { return sinceCling_ < .025f; }
    TuxSliding* GetSlider() const { return slider_; }
    bool HeadOn(const Vector3& direction) const
    {
        if (!sliding_)
            return false;
        else
            return -slider_->GetNode()->GetChild("Graphics")->GetWorldDirection().NormalizedOrDefault(slider_->GetNode()->GetWorldDirection())
                    .DotProduct(direction) > M_1_SQRT2;
    }

    void Burn();

    void Stun(const String& sampleName = "Hit.wav");
    void EndStun(bool blink = true);
    bool IsStunned(bool includeRecover = false) const
    {
        return includeRecover ? !Equals(stun_, 0.f) : stun_ > 0.f;
    }
    bool IsFainted() const
    {
        return stun_ > STUN_LENGTH;
    }

    bool IsUnderwater() const;

    void PickUp(Item* item);
    bool CanUse(StringHash item);
    void UseItem(StringHash item);
    void Release();

    void HandleAction(int actionId) override;
    void FakeJump();

protected:
    void EndAction(int actionId) override;

private:
    void AddAnimationTriggers();
    void HandleAnimationTrigger(const StringHash, VariantMap& eventData);
    void HandlePostRenderUpdate(const StringHash, VariantMap& eventData);
    void HandleNodeCollision(const StringHash eventType, VariantMap& eventData);
    void NodeOnMovingPlatform(Node* node);

    void HandleHardBump(StringHash eventType, VariantMap& eventData);
    void ClingCheck(const Vector3& move);
    Vector3 AvoidSeams(const Vector3& position) const;

    KinematicCharacterController* kinematicController_;
    TuxSliding* slider_;
    StaticModel* hat_;
    ParticleEmitter* fire_;

    float walkSpeed_;
    bool jumpEnded_;
    bool sliding_;
    bool clinging_;
    float sinceCling_;
    SceneObject* hold_;
    float stun_;
    bool burnt_;
    bool leftCast_;

    // Moving platform data
    MovingData movingData_[2];
};

DRY_EVENT(E_PLAYERHIT, PlayerHit)
{
}

#endif // TUX_H
