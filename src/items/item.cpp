/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../tux.h"
#include "../tuxsliding.h"
#include "../game.h"

#include "item.h"

Item::Item(Context* context): SceneObject(context),
    spinNode_{ nullptr },
    graphicsNode_{ nullptr },
    model_{ nullptr },
    pickupSound_{ "Item.wav" },
    pickupSoundGain_{ .6f },
    hat_{ false }
{
}

void Item::OnNodeSet(Node* node)
{
    if (!node)
        return;

    spinNode_ = node_->CreateChild("Spin");
    spinNode_->Yaw(360.f * randomizer_);
    graphicsNode_ = spinNode_->CreateChild("Graphics");

    model_ = graphicsNode_->CreateComponent<StaticModel>();
    model_->SetCastShadows(true);

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    rb->SetTrigger(true);
    rb->SetKinematic(true);
    rb->SetCollisionLayerAndMask(LAYER(L_ITEM), LAYER(L_PLAYER));

    CollisionShape* collisionShape{ node_->CreateComponent<CollisionShape>() };
    collisionShape->SetSphere(.55f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Item, HandleCollisionStart));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Item, HandleUpdate));
}

void Item::SetHat(const String& name)
{
    graphicsNode_->SetPosition(Vector3::BACK * .125f);
    graphicsNode_->SetScale(4/3.f);
    model_->SetModel(RES(Model, "Models/" + name + ".mdl"));
    model_->ApplyMaterialList();

    hat_ = true;
}

void Item::HandleCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (otherNode->HasComponent<Tux>() || otherNode->HasComponent<TuxSliding>())
        PickUp();
}

void Item::PickUp()
{
    if (!pickupSound_.IsEmpty())
        PlaySample(pickupSound_, 0.f, pickupSoundGain_, false);

    Disable();

    Tux* tux{ GetSubsystem<Game>()->GetWorld().tux_ };
    tux->PickUp(this);
}

void Item::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    if (!GetScene()->IsUpdateEnabled())
        return;

    const float t{ eventData[Update::P_TIMESTEP].GetFloat() };
    spinNode_->Yaw(t * 60.f, TS_WORLD);
    spinNode_->SetPosition(Vector3::UP * Sin(GetScene()->GetElapsedTime() * 200.f + randomizer_ * 360.f) * .1f);
}


void Item::OnSetEnabled() { Component::OnSetEnabled(); }
void Item::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Item::OnMarkedDirty(Node* node) {}
void Item::OnNodeSetEnabled(Node* node) {}
bool Item::Save(Serializer& dest) const { return Component::Save(dest); }
bool Item::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Item::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Item::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Item::GetDependencyNodes(PODVector<Node*>& dest) {}
void Item::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
