/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef HERRING_H
#define HERRING_H

#include "../controllable.h"
#include "item.h"

class Herring: public Item
{
    DRY_OBJECT(Herring, Item);

public:
    static void RegisterObject(Context* context);
    Herring(Context* context);

protected:
    void OnNodeSet(Node* node) override;
};

class RedHerring: public Controllable
{
    DRY_OBJECT(RedHerring, Controllable);

public:
    RedHerring(Context* context);

    void OnNodeSet(Node* node) override;
    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void PostUpdate(float timeStep) override;

    void Activate();

private:
    KinematicCharacterController* kinematicController_;
    bool activated_;
    float speed_;
};

#endif // HERRING_H
