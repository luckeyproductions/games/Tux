/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "herring.h"

void Herring::RegisterObject(Context* context)
{
    context->RegisterFactory<Herring>();
    context->RegisterFactory<RedHerring>();
}

Herring::Herring(Context* context): Item(context)
{
}

void Herring::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    Item::OnNodeSet(node);

    graphicsNode_->Pitch(17.f);

    model_->SetModel(RES(Model, "Models/Herring.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/VColOutlineGlossy.xml"));
    model_->SetMaterial(1, RES(Material, "Materials/Brass.xml"));
}

// Used
RedHerring::RedHerring(Context* context): Controllable(context),
    kinematicController_{ nullptr },
    activated_{ false },
    speed_{ 0.f }
{
}

void RedHerring::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);

    model_->SetModel(RES(Model, "Models/Herring.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/VColOutlineGlossy.xml"));
    model_->SetMaterial(1, RES(Material, "Materials/Brass.xml"));
    animCtrl_->PlayExclusive("Animations/Herring/Idle.ani", 0, true);
    animCtrl_->SetStartBone("Animations/Herring/Idle.ani", "Spine0");

    rigidBody_->SetKinematic(true);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetCollisionLayer(LAYER(L_PROJECTILE));
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED - LAYER(L_PLAYER));
    collisionShape_->SetConvexHull(RES(Model, "Models/Tux_COLLISION.mdl"), 0, Vector3::ONE * .55f);

    kinematicController_ = node_->CreateComponent<KinematicCharacterController>();
    kinematicController_->SetTransform(node_->GetPosition(), node_->GetRotation());
    kinematicController_->SetGravity(Vector3::DOWN * 17.f);
    kinematicController_->SetJumpSpeed(5.f);
    kinematicController_->SetCollisionLayer(LAYER(L_PROJECTILE));
    kinematicController_->SetCollisionMask(M_MAX_UNSIGNED - LAYER(L_PLAYER));
    kinematicController_->SetMaxSlope(47.5f);
    kinematicController_->SetStepHeight(.17f);
}

void RedHerring::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    speed_ = 0.f;
}

void RedHerring::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (activated_)
        speed_ = Lerp(speed_, 1.7f, timeStep);

    animCtrl_->SetSpeed("Animations/Herring/Run.ani", 3.6f * Sqrt(kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() * 2.3f));
}

void RedHerring::FixedUpdate(float timeStep)
{
    if (!activated_)
        return;

    const Vector3 targetWalkDir{ move_ * timeStep };
    kinematicController_->SetWalkDirection(speed_ * targetWalkDir);
}

void RedHerring::PostUpdate(float timeStep)
{
    node_->SetPosition(node_->GetPosition().Lerp(kinematicController_->GetPosition(), Min(1.f, 34.f * timeStep)));
}

void RedHerring::Activate()
{
    SetMove(node_->GetWorldDirection());
    animCtrl_->PlayExclusive("Animations/Herring/Run.ani", 0, true, .125f);
    animCtrl_->SetStartBone("Animations/Herring/Run.ani", "Spine0");

    activated_ = true;
}
