/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../creatures/creature.h"
#include "../blockmap/blockmap.h"

#include "wizardhat.h"

PODVector<Node*> WizardHat::frostNodes_{};

void WizardHat::RegisterObject(Context* context)
{
    context->RegisterFactory<WizardHat>();
    context->RegisterFactory<Fireball>();
    context->RegisterFactory<Frost>();
}

WizardHat::WizardHat(Context* context): Item(context)
{
}

void WizardHat::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    Item::OnNodeSet(node);
    SetHat(GetTypeName());
}

bool WizardHat::Freeze(Scene* scene, const Ray& ray)
{
    Node* particleNode{ scene->CreateChild("Freeze") };
    const Vector3 particleDirection{ ray.direction_ * Vector3{ 1.f, .75f, 1.f } };
    particleNode->SetPosition(ray.origin_ + particleDirection);
    particleNode->LookAt(particleNode->GetWorldPosition() + particleDirection);
    ParticleEmitter* coldEmitter{ particleNode->CreateComponent<ParticleEmitter>() };
    coldEmitter->SetEffect(scene->RES(ParticleEffect, "Particles/Freeze.xml"));
    coldEmitter->SetAutoRemoveMode(REMOVE_NODE);

    PhysicsWorld* physics{ scene->GetComponent<PhysicsWorld>() };

    PODVector<RigidBody*> results{};
    float along{ .5f };
    for (int s{ 0 }; s < 6; ++s)
    {
        const float radius{ (s + 1) * .125f};
        const Vector3 position{ ray.origin_ + ray.direction_ * along };
        physics->GetRigidBodies(results, { position, radius }, LAYER(L_ENEMY));

        along += radius;
    }

    bool frozen{ false };
    for (RigidBody* rb: results)
    {
        Creature* creature{ rb->GetNode()->GetDerivedComponent<Creature>() };
        if (creature)
        {
            creature->Freeze();
            frozen = true;
        }
    }

    PhysicsRaycastResult result{};
    if (physics->SphereCast(result, { ray.origin_, ray.direction_ }, .125f, M_SQRT3, LAYER(L_STATIC)))
    {
        Node* resultNode{ result.body_->GetNode() };

        if (resultNode->GetName() == "Liquid")
        {
            AddFrost(scene, VectorRoundToInt(result.position_));
            frozen = true;
        }
        else if (Frost* frost{ resultNode->GetComponent<Frost>() })
        {
            frost->ResetAge();
            frozen = true;
        }
    }

    return frozen;
}

void WizardHat::AddFrost(Scene* scene, const IntVector3& position)
{
    const IntVector3 roundedPos{ position };
    Node* frostNode{ scene->CreateChild("Frost") };
    frostNode->SetPosition(roundedPos);
    frostNode->CreateComponent<Frost>();

    frostNodes_.Push(frostNode);
}

void Frost::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->Yaw(Random(4) * 90.f);
    StaticModel* frost{ node_->CreateComponent<StaticModel>() };
    frost->SetModel(RES(Model, "Models/Frost.mdl"));
    const String materialName{ (GetSubsystem<BlockMap>()->HasWater() ? "Ice" : "Rock") + String{ ".xml" } };
    frost->SetMaterial(RES(Material, "Materials/" + materialName));
    frost->SetCastShadows(true);
    node_->CreateComponent<RigidBody>();
    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetMargin(.025f);
    collider->SetBox(Vector3::ONE, Vector3::DOWN * collider->GetMargin());
}

void Frost::Update(float timeStep)
{
    age_ += timeStep;

    if (age_ >= 10.f)
        Thaw();
}

void Frost::Thaw()
{
    WizardHat::RemoveFrost(node_);
}
