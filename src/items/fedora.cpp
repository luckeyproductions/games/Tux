/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../tux.h"
#include "../tuxsliding.h"
#include "../creatures/creature.h"
#include "../environs/whipgrip.h"

#include "fedora.h"

void Fedora::RegisterObject(Context* context)
{
    context->RegisterFactory<Fedora>();
    context->RegisterFactory<Bullwhip>();
}

Fedora::Fedora(Context* context): Item(context)
{
}

void Fedora::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    Item::OnNodeSet(node);
    SetHat(GetTypeName());
}

// Used
Bullwhip::Bullwhip(Context* context): SceneObject(context),
    grip_{ nullptr },
    curl_{ nullptr },
    tail_{ nullptr },
    target_{ nullptr },
    gripped_{ nullptr },
    stretch_{ 0.f },
    expired_{ false }
{
}

void Bullwhip::OnNodeSet(Node* node)
{
    if (!node)
    {
        if (gripped_)
            gripped_->GetConstraint()->SetOtherBody(nullptr);

        return;
    }

    SceneObject::OnNodeSet(node);

    for (int w{ 0 }; w < 3; ++w)
    {
        AnimatedModel* section{ node_->CreateChild("WhipSection")->CreateComponent<AnimatedModel>() };
        String modelName{ "Whip" };
        switch (w)
        {
        case 0: grip_ = section; modelName += "Grip"; break;
        case 1: curl_ = section; modelName += "Curl"; break;
        case 2: tail_ = section; modelName += "Tail"; break;
        default: break;
        }

        section ->SetModel(RES(Model, "Models/" + modelName + ".mdl"));
        section ->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
        section ->SetCastShadows(true);
    }

    node_->SetEnabledRecursive(true);

    PlaySample("WhipStart.wav", 0.f, .5f, false);
    FindTarget();

    // Debug
//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Bullwhip, HandlePostRenderUpdate));
}

void Bullwhip::FindTarget()
{
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };

    RigidBody* tempBody{ node_->CreateComponent<RigidBody>() };
    tempBody->SetKinematic(true);
    tempBody->SetTrigger(true);
    tempBody->SetCollisionLayerAndMask(LAYER(L_PROJECTILE), LAYER(L_ENEMY) | LAYER(L_WHIPGRIP));
    CollisionShape* tempCollider{ node_->CreateComponent<CollisionShape>() };
    tempCollider->SetBox({ 2.3f, 5.5f, WHIP_REACH }, { 0.f, 1.7f, WHIP_REACH * .5f });
    PODVector<RigidBody*> result{};
    physics->GetRigidBodies(result, tempBody);

    const Vector3 whipPos{ node_->GetWorldPosition() };
    Node* nearest{ nullptr };
    float nearestWeight{ M_INFINITY };

    for (RigidBody* rb: result)
    {
        Node* otherNode{ rb->GetNode() };
        const bool isCreature{ otherNode->GetDerivedComponent<Creature>() != nullptr };
        const bool isWhipGrip{ otherNode->GetParentComponent<WhipGrip>() != nullptr };
        const Vector3 delta{ otherNode->GetWorldPosition() - node_->GetWorldPosition() };
        const float angle{ node_->GetWorldDirection().Angle(delta)};

        if ((!isCreature && !isWhipGrip) || angle > 70.f)
            continue;

        if (!nearest)
        {
            nearest = otherNode;
        }
        else
        {
            const float squaredDist{ otherNode->GetWorldPosition().DistanceSquaredToPoint(whipPos) };
            const float weight{ squaredDist + Sqrt(angle) };

            if (weight < nearestWeight)
            {
                nearest = otherNode;
                nearestWeight = weight;
            }
        }
    }

    if (nearest)
    {
        target_ = nearest;
        const Vector3 parentUp{ node_->GetParent()->GetWorldUp() };
        node_->LookAt(target_->GetWorldPosition() + parentUp * (WHIP_RADIUS + .55f), parentUp);
    }

    tempBody->Remove();
    tempCollider->Remove();
}

bool Bullwhip::HitCheck(PhysicsRaycastResult& result)
{
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };

    RigidBody* tempBody{ node_->CreateComponent<RigidBody>() };
    tempBody->SetKinematic(true);
    tempBody->SetTrigger(true);
    tempBody->SetCollisionLayer(LAYER(L_PROJECTILE));
    tempBody->SetCollisionMask(LAYER(L_STATIC) | LAYER(L_ENEMY) | LAYER(L_WHIPGRIP));
    CollisionShape* tempCollider{ node_->CreateComponent<CollisionShape>() };
    tempCollider->SetBox({ 2.f * WHIP_RADIUS, 2.f * WHIP_RADIUS, stretch_ }, { 0.f, 0.f, stretch_ * .5f });
    PODVector<RigidBody*> res{};
    physics->GetRigidBodies(res, tempBody);

    const Vector3 whipPos{ node_->GetWorldPosition() };
    Node* nearest{ nullptr };
    for (RigidBody* rb: res)
    {
        Node* otherNode{ rb->GetNode() };

        if (!nearest)
            nearest = otherNode;
        else if (nearest->GetWorldPosition().DistanceSquaredToPoint(whipPos) >
                 otherNode->GetWorldPosition().DistanceSquaredToPoint(whipPos))
            nearest = otherNode;
    }

    tempBody->Remove();
    tempCollider->Remove();

    result = PhysicsRaycastResult{};
    if (nearest)
        result.body_ = nearest->GetComponent<RigidBody>();
    else
        result.body_ = nullptr;

    return nearest != nullptr;
}

void Bullwhip::PostUpdate(float timeStep)
{
    if (!node_->IsEnabled())
        return;

    if (expired_)
    {
        node_->GetParent()->GetComponent<Tux>()->Release();
        return;
    }

    if (!gripped_)
    {
        stretch_ += stretch_ * timeStep * 7.f + timeStep * 42.f;
        if (stretch_ > WHIP_REACH)
            stretch_ = WHIP_REACH;
    }
    else
    {
        const Vector3 gripPos{ gripped_->GetNode()->GetWorldPosition() };
        node_->LookAt(gripPos);
        stretch_ = node_->GetWorldPosition().DistanceToPoint(gripPos);
    }

    const float progress{ stretch_ / WHIP_REACH };
    grip_->SetMorphWeight(0, stretch_);

    const Vector3 curlPos{ Vector3::FORWARD * stretch_ };
    const float curlScale{ 1.f - Sqrt(progress) };
    Node* curlNode{ curl_->GetNode() };
    curlNode->SetPosition(curlPos);
    curlNode->SetScale(curlScale);
    curl_->SetMorphWeight(0, 1 / Min(curlScale, M_LARGE_VALUE) );

    if (!gripped_)
    {
        Node* tailNode{ tail_->GetNode() };
        tailNode->SetPosition(curlScale * Vector3{ .274561f, -1.028899f , -1.276341f } + curlPos);
        tail_->SetMorphWeight(0, (WHIP_REACH - 5.f * curlScale - stretch_) / 8);
    }
}

void Bullwhip::FixedUpdate(float timeStep)
{
    if (expired_ || gripped_)
        return;

    PhysicsRaycastResult result{};
    const bool hit{ HitCheck(result) };

    if (hit)
    {
        Node* hitNode{ result.body_->GetNode() };
        if (!hitNode)
            return;

        Creature* creature{ hitNode->GetDerivedComponent<Creature>() };
        if (creature)
        {
            creature->WhipHit((hitNode->GetWorldPosition() - node_->GetWorldPosition()).NormalizedOrDefault());
        }
        else
        {
            WhipGrip* whipGrip{ hitNode->GetParentComponent<WhipGrip>() };
            if (whipGrip)
                Grip(whipGrip);
        }
    }

    if (hit || stretch_ >= WHIP_REACH)
    {
        PlaySample("WhipCrack.wav", 0.f, .7f, false);

        if (!gripped_)
            expired_ = true;
    }
}

void Bullwhip::Grip(WhipGrip* whipGrip)
{
    gripped_ = whipGrip;

    if (gripped_)
    {
        // Set wrapped curl
        curl_->SetModel(nullptr);

        // Hide tail (?)
        Node* tailNode{ tail_->GetNode() };
        tailNode->SetEnabled(false);

        // Constrain Tux
        Tux* tux{ node_->GetParentComponent<Tux>() };
        tux->StartSliding();
        TuxSliding* slider{ tux->GetSlider() };
        Node* graphicsNode{ slider->GetNode()->GetChild("Graphics") };
        AnimationController* animCtrl{ graphicsNode->GetComponent<AnimationController>() };
        animCtrl->PlayExclusive("Animations/Swing.ani", 0, true, 0.17f);
        animCtrl->SetStartBone("Animations/Swing.ani", "Hips");

        RigidBody* sliderBody{ slider->GetComponent<RigidBody>() };
        sliderBody->SetAngularFactor(Vector3::ONE);
        Node* sliderNode{ sliderBody->GetNode() };
        Quaternion sliderRot{ sliderNode->GetWorldRotation() };
        Constraint* gripConstraint{ gripped_->GetConstraint() };
        gripConstraint->SetOtherPosition(sliderRot.Inverse() * (gripped_->GetNode()->GetWorldPosition() - tux->GetNode()->GetWorldPosition()));
        gripConstraint->SetOtherBody(sliderBody);
        gripConstraint->SetOtherRotation(sliderRot);

        ValueAnimation* posAnim{ new ValueAnimation(context_) };
        posAnim->SetKeyFrame(0.f, node_->GetPosition());
        posAnim->SetKeyFrame(.1f, Vector3{ .12f, .9f, .3f });
        node_->SetAttributeAnimation("Position", posAnim);
        node_->SetAttributeAnimationWrapMode("Position", WM_ONCE);
    }
    else
    {
        // Set initial curl
//        curl_->SetModel();

        // Show tail
        Node* tailNode{ tail_->GetNode() };
        tailNode->SetEnabled(true);
    }
}

void Bullwhip::HandlePostRenderUpdate(const StringHash eventType, VariantMap& eventData)
{
    // Debug raycasting
    for (bool r: { false, true })
    {
        const Ray whipRay{ GetWhipRay(r * WHIP_RADIUS) };
        const Vector3 from{ whipRay.origin_ };
        const Vector3 to{ from + whipRay.direction_ * stretch_ };
        DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
        debug->AddLine(from, to, r ? Color::WHITE : Color::RED, false);
        if (r)
        {
            debug->AddSphere({ from, WHIP_RADIUS }, Color::YELLOW);
            debug->AddSphere({ to, WHIP_RADIUS }, Color::YELLOW);
        }
    }
}
