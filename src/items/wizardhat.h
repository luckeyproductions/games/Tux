/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WIZARDHAT_H
#define WIZARDHAT_H

#include "item.h"

#include "fireball.h"

class Frost: public LogicComponent
{
    DRY_OBJECT(Frost, LogicComponent);

public:
    Frost(Context* context): LogicComponent(context),
        age_{ 0.f }
    {
        SetUpdateEventMask(USE_UPDATE);
    }

    void Update(float timeStep) override;

    void ResetAge()
    {
        age_ = 0.f;
    }

    void Thaw();

protected:
    void OnNodeSet(Node* node) override;

private:
    float age_;

};

class WizardHat: public Item
{
    DRY_OBJECT(WizardHat, Item);

public:
    static void Clear()
    {
        for (Node* n: frostNodes_)
            n->Remove();

        frostNodes_.Clear();
    }

    static void RemoveFrost(Node* node)
    {
        frostNodes_.Remove(node);
        node->Remove();
    }

    static void RegisterObject(Context* context);
    static bool Freeze(Scene* scene, const Ray& ray);

    WizardHat(Context* context);

protected:
    void OnNodeSet(Node* node) override;

private:
    static void AddFrost(Scene* scene, const IntVector3& position);

    static PODVector<Node*> frostNodes_;
};

#endif // WIZARDHAT_H
