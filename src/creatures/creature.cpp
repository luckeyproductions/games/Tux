/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"
#include "../tux.h"
#include "../items/herring.h"
#include "../effect/poof.h"

#include "creature.h"

Creature::Creature(Context* context): Controllable(context),
    kinematicController_{ nullptr },
    dirCheckRadius_{ .5f },
    dirCheckDist_{ 1.f },
    wide_{ false },
    speed_{ 0.f },
    traveled_{ 0.f },
    stun_{ 0.f },
    frozen_{ false },
    chaseHerring_{ true },
    topHitEffect_{ HE_NONE },
    sideHitEffect_{ HE_HURT },
    lethalStun_{ true },
    sliding_{ false }
{
}

void Creature::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);

    rigidBody_->SetKinematic(true);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetCollisionLayerAndMask(LAYER(L_ENEMY), M_MAX_UNSIGNED - LAYER(L_ITEM));

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Creature, HandleNodeCollision));
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Creature, HandleNodeCollision));
}

void Creature::Set(const Vector3& position, const Quaternion& rotation)
{
    Controllable::Set(position, rotation);
    EndStun();

    if (kinematicController_)
    {
        kinematicController_->SetTransform(position, rotation);
        kinematicController_->SetEnabled(true);
    }
}

void Creature::Disable()
{
    Controllable::Disable();

    if (kinematicController_)
    {
        kinematicController_->Remove();
        kinematicController_ = nullptr;
    }
}

void Creature::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (stun_ > 0.f)
    {
        stun_ -= timeStep;
        if (stun_ <= 0.f)
            EndStun();
    }
}

void Creature::CreateKinematicController()
{
    if (!kinematicController_)
        kinematicController_ = node_->CreateComponent<KinematicCharacterController>();
    kinematicController_->SetTransform(node_->GetPosition(), node_->GetRotation());
    kinematicController_->SetCollisionLayer(LAYER(L_ENEMY));
    kinematicController_->SetCollisionMask(M_MAX_UNSIGNED);
    kinematicController_->SetGravity(Vector3::DOWN * 17.f);
    kinematicController_->SetJumpSpeed(0.f);
    kinematicController_->SetMaxSlope(47.5f);
    kinematicController_->SetStepHeight(.23f);
}

void Creature::Halt()
{
    speed_ = 0.f;
    SetMove(Vector3::ZERO);
}

PODVector<Vector3> Creature::GetAvailableDirections() const
{
    PODVector<Vector3> directions{};

    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    const Vector3 worldPos{ node_->GetWorldPosition() + Vector3::UP * 3/4.f };

    for (int d{ 0 }; d < 4; ++d)
    {
        const Quaternion rot{ 90.f * d, Vector3::UP };
        const Vector3 forward{ VectorRound(rot * Vector3::FORWARD) };
        const Vector3 right{ rot * Vector3::RIGHT };
        bool hit{ false };
        PODVector<int> offsets{ 0 };
        if (wide_)
            offsets.Push({ -1, 1 });

        for (int o: offsets)
        {
            PhysicsRaycastResult result{};
            const unsigned mask{ 1u };
            const float dist{ dirCheckDist_ - dirCheckRadius_ };
            const Ray forwardRay{ worldPos + right * dist * .8f * o + forward * dirCheckRadius_, forward };
            physics->SphereCast(result, forwardRay, dirCheckRadius_, dist - dirCheckRadius_, mask);

            if (result.body_)
            {
                if (result.distance_ < dirCheckDist_)
                {
                    hit = true;
                    break;
                }
            }
        }

        if (!hit)
            directions.Push(forward);
    }

    return directions;
}

bool Creature::CliffCheck(const Vector3& direction) const
{
    PhysicsRaycastResult result{};
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    const unsigned mask{ 1u };
    const Vector3 worldPos{ node_->GetWorldPosition() + Vector3::UP * 1.f };
    const Vector3 rayOrigin{ worldPos + (direction == Vector3::ZERO ? !Equals(speed_, .0f) * 2.f * move_.NormalizedOrDefault(Vector3::ZERO)
                                                                    : direction) };
    const float radius{ 2/3.f };
    physics->SphereCast(result, { rayOrigin, Vector3::DOWN }, radius, 1.f - radius + kinematicController_->GetStepHeight(), mask);

    return result.body_ == nullptr;
}

Vector3 Creature::TargetCheck()
{
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    Vector3 target{ Vector3::ZERO };

    for (int d{ 0 }; d < 4; ++d)
    {
        const Quaternion rot{ 90.f * d, Vector3::UP };
        const Vector3 forward{ VectorRound(rot * Vector3::FORWARD) };

        if (forward.Angle(node_->GetWorldDirection()) > 120.f)
            continue;

        const Vector3 worldPos{ node_->GetWorldPosition() + Vector3::UP * .5f };

        PhysicsRaycastResult result{};
        const Ray forwardRay{ worldPos, forward };
        physics->SphereCast(result, forwardRay, 1.25f, 9.f, LAYER(L_PLAYER) | LAYER(L_PROJECTILE));

        if (result.body_)
        {
            if ((target == Vector3::ZERO && result.body_->GetCollisionLayer() == LAYER(L_PLAYER)) ||
                (chaseHerring_ && result.body_->GetNode()->HasComponent<RedHerring>()))
            {
                const Vector3 targetDelta{ result.body_->GetNode()->GetWorldPosition() - node_->GetWorldPosition() };
                target = targetDelta.ProjectOntoPlane(forward.CrossProduct(Vector3::UP)).ProjectOntoPlane(Vector3::UP);
            }
        }
    }

    return target;
}

void Creature::Stun()
{
    Halt();
    stun_ = 7.f;

    if (lethalStun_)
        topHitEffect_ = HE_DIE;

    sideHitEffect_ = HE_NONE;

    ValueAnimation* squeeze{ new ValueAnimation{ context_ } };
    squeeze->SetValueType(VAR_VECTOR3);
    squeeze->SetInterpolationMethod(IM_SINUSOIDAL);
    const float press{ 1/2.7f };
    const float bulge{ Sqrt(1/press) };
    const Vector3 initialScale{ graphicsNode_->GetScale() };
    const Vector3 endScale{ Vector3::ONE };
    const Vector3 fullSqueeze{ bulge, press, bulge };
    const float dur{ .42f };
    const float dt{ dur / 4.f };
    bool pos{ true };

    for (float t{ 0.f }; t < dur; t += dt)
    {
        const float nt{ t / dur };
        const Vector3 interScale{ initialScale.Lerp(endScale, nt) };
        squeeze->SetKeyFrame(t, interScale.Lerp(fullSqueeze, (1.f - pos) * (1.f - nt)));
        pos = !pos;
    }

    squeeze->SetKeyFrame(dur, endScale);
    graphicsNode_->SetAttributeAnimation("Scale", squeeze, WM_ONCE);

    if (kinematicController_)
        kinematicController_->SetWalkDirection(Vector3::ZERO);
}

void Creature::EndStun()
{
    stun_ = 0.f;
    frozen_ = false;

    if (lethalStun_)
        topHitEffect_ = HE_STUN;

    sideHitEffect_ = HE_HURT;
    graphicsNode_->RemoveAttributeAnimation("Scale");
}

void Creature::Perish()
{
    Node* poofNode{ GetScene()->CreateChild("Poof") };
    poofNode->SetWorldPosition(node_->GetWorldPosition());
    poofNode->CreateComponent<Poof>();
    PlaySample("Poof.wav", 0.f, .8f, false);

    Disable();
}

void Creature::WhipHit(const Vector3& direction)
{
    if (IsStunned())
        Perish();
    else
        Stun();
}

void Creature::Freeze()
{
    stun_ = 10.f;
    if (frozen_)
        return;

    Halt();
    frozen_ = true;
    topHitEffect_  = HE_NONE;
    sideHitEffect_ = HE_NONE;

    model_->SetMaterial(model_->GetMaterial()->Clone());
    model_->GetMaterial()->SetTechnique(0, model_->GetMaterial()->GetTechnique(0)->CloneWithDefines("", "FROZEN"));

    for (const AnimationControl& a: animCtrl_->GetAnimations())
        animCtrl_->SetSpeed(a.name_, 0.f);

    if (kinematicController_)
        kinematicController_->SetWalkDirection(Vector3::ZERO);
}

void Creature::HandleNodeCollision(const StringHash eventType, VariantMap& eventData)
{
    using namespace NodeCollision;

    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[P_OTHERBODY].GetVoidPtr()) };
    if (!otherBody)
        return;

    Node* otherNode{ otherBody->GetNode() };
    if (otherNode->HasTag("Lava"))
    {
        if (IsFrozen())
            EndStun();
        else
            Perish();
    }

    //    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    bool fromTop{ false };

//    while (!contacts.IsEof())
//    {
//        // Read position and normal
//        contacts.ReadVector3();
//        const Vector3 normal{ contacts.ReadVector3() };
//        if (otherNode->GetWorldPosition().Angle(Vector3::DOWN) < 55.f)
//            fromTop = true;
//    }
    const Vector3 delta{ otherNode->GetWorldPosition() - node_->GetWorldPosition() };
    if (delta.Normalized().y_ > M_1_SQRT2)
        fromTop = true;

    const bool hitTux{ otherNode->HasComponent<Tux>() || otherNode->HasComponent<TuxSliding>() };
    if (hitTux)
    {
        Tux* tux{ GetSubsystem<Game>()->GetWorld().tux_ };
        KinematicCharacterController* tuxController{ tux->GetComponent<KinematicCharacterController>() };
        const Vector3 lv{ (tux->IsSliding() ? tux->GetSlider()->GetNode()->GetComponent<RigidBody>()->GetLinearVelocity()
                                            : tuxController->GetLinearVelocity()) };
        if (lv.y_ <= 0.f)
            fromTop |= (delta.y_ - .5f * tux->IsSliding()) > -lv.y_ * .05f;

        HitEffect effect{ (fromTop ? topHitEffect_ : sideHitEffect_) };
        const bool collStart{ eventType == E_NODECOLLISIONSTART };
        const bool headbutt{ tux->HasToughHat() && tux->HeadOn(delta.Normalized()) };
        if (headbutt)
        {
            if (IsStunned() && lethalStun_)
            {
                effect = HE_DIE;
            }
            else if (effect != HE_DIE)
            {
                effect = HE_STUN;
                tux->EndSliding();
            }
        }

        if (fromTop && collStart && !node_->HasTag("MovingPlatform"))
        {
            tuxController->Jump(Vector3::UP * (7.f + tux->Bounce() * 5.f));
            if (!tux->IsStunned())
                tux->FakeJump();
        }

        switch (effect)
        {
        case HE_DIE:  if (collStart) Perish(); break;
        case HE_STUN: if (collStart) Stun();   break;
        case HE_HURT:
        {
            if (tux->IsStunned(true))
                return;

            const Vector3 away{ delta.Normalized() };
            Quaternion rot{};
            rot.FromLookRotation(-away.ProjectOntoPlane(Vector3::UP).Normalized());

            tux->Stun();
            tuxController->SetWalkDirection(Vector3::ZERO);
            tuxController->ApplyImpulse(5.5f * away + 2.3f * Vector3::UP);
            tux->GetNode()->SetWorldDirection(-away.ProjectOntoPlane(Vector3::UP));
        }
        break;
        case HE_SLIDE: /*StartSlide(delta.NormalizedOrDefault(tux->GetNode()->GetWorldDirection())); break;*/
        case HE_NONE: default: break;
        }
    }
}
