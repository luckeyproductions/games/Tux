/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BULLHORN_H
#define BULLHORN_H

#include "creature.h"

class Bullhorn: public Creature
{
    DRY_OBJECT(Bullhorn, Creature);

public:
    Bullhorn(Context* context);

    void OnNodeSet(Node* node) override;
    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void PostUpdate(float timeStep) override;

    void Halt() override;
    void Stun() override;
    void WhipHit(const Vector3& direction) override;

protected:
    void EndStun() override;
    void CreateKinematicController() override;

private:
    void Storm(const Vector3& target);

    bool storming_;
};

#endif // BULLHORN_H
