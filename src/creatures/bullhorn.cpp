/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../items/herring.h"
#include "../effect/stars.h"

#include "bullhorn.h"

Bullhorn::Bullhorn(Context* context): Creature(context),
    storming_{ false }
{
    dirCheckRadius_ = 2/3.f;
    dirCheckDist_ = 1.5f;
    wide_ = true;
    traveled_ = -1.f;
    lethalStun_ = false;
}

void Bullhorn::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Creature::OnNodeSet(node);

    node_->AddTag("MovingPlatform");

    model_->SetModel(RES(Model, "Models/Bullhorn.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/VColOutline.xml"));
    animCtrl_->PlayExclusive("Animations/Bullhorn/Idle.ani", 0, true);

    collisionShape_->SetConvexHull(RES(Model, "Models/Bullhorn_COLLISION.mdl"));
    collisionShape_->SetMargin(.08f);
}

void Bullhorn::Set(const Vector3& position, const Quaternion& rotation)
{
    CreateKinematicController();

    Creature::Set(position, rotation);

    speed_ = 0.f;
    traveled_ = 0.f;
    SetMove(Vector3::ZERO);
}

void Bullhorn::CreateKinematicController()
{
    Creature::CreateKinematicController();
    kinematicController_->SetMaxSlope(42.f);
    kinematicController_->SetStepHeight(.17f);
}

void Bullhorn::Update(float timeStep)
{
    Creature::Update(timeStep);

    if (IsStunned())
        return;

    if (traveled_ >= 0.f)
    {
        const float targetSpeed{ 1.23f * !Equals(move_.LengthSquared(), 0.f) };
        speed_ = Lerp(speed_, targetSpeed, timeStep);
    }
    else if (storming_)
    {
        const float targetSpeed{ 7.f };
        speed_ = Lerp(speed_, targetSpeed, timeStep * 5.f);
    }

    if (!Equals(speed_, 0.f))
    {
        float moveDot{ move_.Normalized().DotProduct(node_->GetWorldDirection()) };
        if (storming_)
        {
            animCtrl_->PlayExclusive("Animations/Bullhorn/Run.ani", 0, true, 1/6.f);
            animCtrl_->SetSpeed("Animations/Bullhorn/Run.ani", moveDot * 2.3f * Sqrt(kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() * 2.3f));
        }
        else
        {
            animCtrl_->PlayExclusive("Animations/Bullhorn/Walk.ani", 0, true, 2/6.f);
            animCtrl_->SetSpeed("Animations/Bullhorn/Walk.ani", moveDot * 4.5f * Sqrt(kinematicController_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP).Length() * 2.3f));
        }
    }
    else
    {
        animCtrl_->PlayExclusive("Animations/Bullhorn/Idle.ani", 0, true, 1/6.f);
    }

    if (traveled_ < 0.f)
    {
        traveled_ += timeStep;
        if (traveled_ > 0.f)
            traveled_ = 0.f;
    }
}

void Bullhorn::Halt()
{
    Creature::Halt();

    traveled_ = -Random(Sqrt(Max(0.f, traveled_)));
    storming_ = false;
}

void Bullhorn::Stun()
{
    Creature::Stun();

    if (!kinematicController_)
        return;

    kinematicController_->Remove();
    kinematicController_ = nullptr;
    collisionShape_->SetSize({ 1.f, .75f, 1.f });
    rigidBody_->SetTrigger(false);

    const String stunnedAnim{ "Animations/Bullhorn/Stunned.ani" };
    animCtrl_->PlayExclusive(stunnedAnim, 0, true, .1f);
    animCtrl_->SetSpeed(stunnedAnim, 2.3f);
    animCtrl_->SetLooped(stunnedAnim, false);

    if (node_->GetChild("Stars") == nullptr)
    {
        Node* starsNode{ node_->CreateChild("Stars") };
        starsNode->Translate({ 0.f, 1.7f, .55f });
        starsNode->CreateComponent<Stars>();
    }
}

void Bullhorn::WhipHit(const Vector3& direction)
{
    if (!storming_)
        Storm(node_->GetWorldDirection() * 5.f);
    else
        Stun();
}

void Bullhorn::EndStun()
{
    Creature::EndStun();

    model_->SetMaterial(0, RES(Material, "Materials/VColOutline.xml"));

    collisionShape_->SetSize(Vector3::ONE);
    rigidBody_->SetTrigger(true);
    CreateKinematicController();

    Node* starNode{ node_->GetChild("Stars") };
    if (starNode)
        starNode->Remove();

    Halt();
}

void Bullhorn::FixedUpdate(float timeStep)
{
    if (!kinematicController_ || IsStunned())
        return;

    if (Equals(speed_, 0.f) && traveled_ >= 0.f)
    {
        auto dirs{ GetAvailableDirections() };
        if (!dirs.IsEmpty())
            SetMove(dirs.At(Random(static_cast<int>(dirs.Size()))));
    }
    else if (!storming_)
    {
        if (!GetAvailableDirections().Contains(move_))
        {
            node_->Translate(-speed_ * node_->GetWorldDirection() * timeStep);

            if (move_.LengthSquared() > 0.f)
                Halt();
        }
    }

    if (traveled_ > -1.f)
    {
        const Vector3 target{ TargetCheck() };
        if (target != Vector3::ZERO)
            Storm(target);
    }

    const Vector3 rightMove{ move_.CrossProduct(Vector3::UP) };
    const Vector3 roundPos{ VectorRound(node_->GetWorldPosition()) };
    const Vector3 offPos{ roundPos - node_->GetWorldPosition() };
    const Vector3 lineRight{ rightMove * rightMove.Normalized().DotProduct(offPos) };
    const Vector3 targetWalkDir{ (move_ + lineRight) * timeStep };
    kinematicController_->SetWalkDirection(speed_ * targetWalkDir);
}

void Bullhorn::PostUpdate(float timeStep)
{
    if (!kinematicController_ || IsStunned())
        return;

    const Vector3 oldPos{ node_->GetPosition() };
    const Vector3 newPos{ oldPos.Lerp(kinematicController_->GetPosition(), Min(1.f, 34.f * timeStep)) };
    const Vector3 delta{ newPos - oldPos };
    const float dist{ delta.Length() };

    node_->SetPosition(newPos);
    traveled_ += dist;

    if (!Equals(speed_, 0.f))
    {
        if ((storming_ && traveled_ >= 0.f)
            || dist < .1f * timeStep * speed_ || traveled_ > 7.f || CliffCheck())
        {
            Halt();
        }
        else
        {
            const Vector3 dir{ node_->GetWorldDirection() };
            node_->Yaw(Equals(dir.Angle(delta), 180.f, .55f)
                       ? RandomSign()
                       : (dir.CrossProduct(move_).y_ * timeStep * 420.f));
            kinematicController_->SetTransform(kinematicController_->GetPosition(), node_->GetWorldRotation());
        }
    }
}

void Bullhorn::Storm(const Vector3& target)
{
    if (Equals(target.LengthSquared(), 0.f))
        return;

    storming_ = true;
    traveled_ = -(target.Length() + 3.f);
    SetMove(target.Normalized());
}
