/* Tux!
// Copyright (C) 2018-2025 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "shell.h"

Shell::Shell(Context* context): LogicComponent(context),
    rigidBody_{ nullptr },
    collisionShape_{ nullptr },
    smoothVelocity_{}
{
}

void Shell::OnNodeSet(Node* node)
{
    if (!node)
        return;

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(2.3f);
    rigidBody_->SetLinearRestThreshold(.05f);
    rigidBody_->SetAngularFactor(Vector3::UP);
    rigidBody_->SetRestitution(1.f);
    rigidBody_->SetLinearDamping(.1f);
    rigidBody_->SetAngularDamping(.5f);
    rigidBody_->SetRollingFriction(.1f);
    rigidBody_->SetCollisionLayer(LAYER(L_PLAYER));
    rigidBody_->SetCollisionMask(M_MAX_UNSIGNED);

    collisionShape_ = node_->CreateComponent<CollisionShape>();
    collisionShape_->SetConvexHull(RES(Model, "Models/Snail_COLLISION.mdl"));
    collisionShape_->SetMargin(.21f);
}

Vector3 Shell::SurfaceNormal()
{
    PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
    PhysicsRaycastResult result{};
    physicsWorld->RaycastSingle(result, { node_->GetWorldPosition(), Vector3::DOWN }, .5f, LAYER(L_STATIC));

    return result.distance_ != M_INFINITY ? result.normal_ : Vector3::UP;
}

void Shell::Update(float timeStep)
{
    Node* graphicsNode{ node_->GetChild("Graphics") };
    const Vector3 groundNormal{ SurfaceNormal() };
    const Vector3 lv{ rigidBody_->GetLinearVelocity() };
    smoothVelocity_ = smoothVelocity_.Lerp(lv, Min(1.f, timeStep * 23.f));
    const float v{ smoothVelocity_.Length() };
    const Vector3 d{ smoothVelocity_.NormalizedOrDefault(graphicsNode->GetWorldDirection().ProjectOntoPlane(groundNormal).Normalized()) };

    const Vector3 front{ graphicsNode->GetWorldPosition() + graphicsNode->GetWorldDirection().Lerp(d, Min(1.f, timeStep * 7.f  * Clamp(PowN(v, 3), 1.f, .23f))) };
    const Vector3 up{ (groundNormal * 5.0f + Vector3::UP).Normalized() };
    graphicsNode->LookAt(front, graphicsNode->GetWorldUp().Lerp(up, Min(1.f, timeStep * 10.f)));
}

void Shell::FixedUpdate(float timeStep)
{

}

void Shell::FixedPostUpdate(float timeStep)
{

}
