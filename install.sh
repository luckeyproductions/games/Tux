#!/bin/sh

./.builddry.sh
export DRY_HOME=$PWD/Dry/build

if [ -x "$(command -v dnf)" ]
then build_command=qmake-qt5
else build_command=qmake
fi

git pull
$build_command Tux.pro -o ../Tux-build/; cd ../Tux-build
sudo make install; cd -; ./.postinstall.sh
rm -rf ../Tux-build
